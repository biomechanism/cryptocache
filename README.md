# CryptoCache

## About


Basic terminal crypto trading automation. Currently the only exchange supported is Binance. Others may be added in time. Market/Spot trading is also the only currently supported functionality.


## Build & Run

This program makes use of the Binance-Java-Api class for exchange connectivity. However the Maven package doesn't appear to be well maintained, and it outdated, as such there is some extra work that need to be done for building this project. The `Binance-Java-Api` project need to be checked out and built. The jar then needs to be copied in the libs directory of this project before performing the build.

I have forked the `Binance-Java-Api` project, as I added an enhancement to allow requesting the underlying `OkHttp` libraries websocket connection to be closed down, my pull request has so far not been accepted. Also there has been some breaking changes since the I forked the library, which I will get to bringing back in line in due course.

```bash
	git clone https://gitlab.com/biomechanism/cryptocache

	git clone https://github.com/biomechanism/binance-java-api
	
	cd binance-java-api/
	
	mvn package
```

This will build the necessary jar, which can now be copied into the project for building.

```bash
	cp target/binance-api-client-1.0.1.jar ../cryptocache/CryptoCache/libs
	
	cd ../cryptocache/CryptoCache/
	
	gradle fatJar
```

The self-contained executable jar will now be generated.
```bash
	cd build/libs/
	
	java -jar ./CryptoCache-all-0.19.jar
```

__NOTE__: The version number of the generated jars may be different, so you will need to substitute those with whatever one are actually generated for you.


* * *

## Config

Currently the program expects a config file under a `.cryptotraders directory` in the users home directory. This will be user configurable a later update.

```bash
~/.cryptotraders/cryptotrader.properties
```

```
email.user = <USER EMAIL ADDRESS> 
email.password = <USER PASSWORD>
email.active = <true | false> 
email.to = <TO EMAIL ADDRESS> 
mail.smtp.host = <SMTP HOST> 
mail.smtp.port = <SMTP PORT> 
mail.smtp.auth = <true | false>
mail.smtp.starttls.enable = <true | false> 
```

### Binance API Keys

The API keys must be in the `.binance` directory which is under the `.cryptotraders` directory. 

The key files should be:

```bash
~/.cryptotrader/.binance/.apiKey  
~/.cryptotrader/.binance/.secretKey
```

## Usage

You can list all available commands by typing `help`, or its alias `h`, and to get a little more detail help followed by the command or it's alias, <command name | alias>

### Create a new trader

For example, to create a new polling trader for the BTC/BUSD pair using the MACD strategy, we could enter the below. Starting with a primary balance of 0.1 BTC, and no BUSD. 

Most trader strategies will start off in BUY mode, which means you need a secondary currency balance to buy the primary currency. 

```bash
Enter a  CMD: new trader    
--ENTER TRADER DETAILS--    
    
Trader Name > BtcBusdTestTrader    
    
Primary Ccy > btc    
    
Secondary Ccy > busd    
    
Primary Balance > 0.1    
    
Secondary Balance > 0    
    
Strategy Name > MACDStrategy    
    
TraderType (polling, realtime, newlisting) Name > polling    
    
Exchange Name > BinanceExchange    
Enter a  CMD: list traders    
BtcBusdTestTrader    
```

A polling trader also need to have the start time set if you don't wan tto wait for the current time +1 day. And that is set with the `set start time` command.

```bash
Enter a  CMD: set start time

Set start hour (0-23) > 19

Set start minute (0-59) > 0 

Set start second (0-59) > 30
OK
```

Setting the seconds to 30 here is a bit of a work-around for when the next candle is expected, as subscribing to notifications has yet to be implemented. This just tries to ensure it picks up the new candle reasonably quick, as if it checked before it would be delayed until the next polling period, and would cause too much of a delay.

Top perform operations that can modify the trader, the user will need to first select the trader, with the `select trader` command.


In the case above we would want the next trade to sell, as there is only a primary balance, so it can be put into sell mode by the following command: 

`set mode sell`

The MACD strategy has a number of attributes, which can be listed with the `show attributes` command, or the alias `attrs`. the `CandlePeriod` specifies which candle period should be used for the strategy. And can be set as follows.

`set attribute`

`CandlePeriod = 1d`

As we are running the polling trader here we also need to ensure it runs at a similar period to the candles we are checked, in this case 1 day, which currently has to be specified in seconds.

```bash
Enter a  CMD: set run period

Enter Run Period (Seconds) > 86400
```


### Starting the Trader

Once the settings are as required, the trader can be started with either the `start trader` or `start all traders` command.

You can check it has been started vis the `show trader details` command, or for more information the log file.


* * *
	

## Primary Components

### Accounts

These are logical accounts that provide a separation from the actual real exchange account. Each trader has an account with a primary and secondary currency pair and balance. The trader will only use what is available in the logical trading account. i.e. If your real exchange account has 20 ETH, and you specify 10 ETH for the traders account, only those 10 ETH will be trade, along with any gains from the trades made. You could, for instance, have a number of traders running different strategies with the full exchange balance distributed amongst them.


### AutoTraders


These are the actual components that are scheduled and ran. They make use of the Strategy and Exchange components for analysing and submitting market orders. Each trader has it's own account, with primary and secondary currency and balance. 
e.g. ETH/USDT, where ETH is the primary balance and USDT is the secondary balance. 

There are two main types of autotraders.

+ Polling
+ Realtime

#### Polling

These traders require a start time to be specified, and a period specified in seconds. The period is the frequency with which the traders runs, i.e. 2 for every two seconds or 86400 for once a day, etc. The start time is specified on a 24 hour clock. If the time is specified in the past, then it is scheduled for that time the following day.

There are currently two types of Binance exchange implemented, and the Polling traders can make use of either type, they are the normal BinanceExchange and the `BinanceWebSocketExchange`. 

If you are running polling traders with a reasonably high period/frequency, then it is best to use the `BinanceWebSocketExchange` so you are not hitting the exchange with a lot of requests and and potentially getting a temporary ban. 

If your polling is not that frequent then the `BinanceExchange` may be the better option, it doesn't need to maintain a web socket connection, so gets rid of the risk of the traders automatically shutting down in case of a prolonged web socket disconnect. 

#### Realtime

Realtime traders receive rate events every time there is a rate change from the exchange component on the pair the autotrader is setup to trade. These traders are best if you want to receive a notification and analyse ever rate change. All realtime traders are limited to using the exchange `BinanceWebSocketExchange`. 

These traders can also suffer if your Internet connection is unstable. If the exchange cannot reestablish he websocket connection within a specified number of attempts (default 5) then these traders will be shutdown, and will need to be restarted manually.



### Exchanges

#### BinanceExchange

This exchange can be used if there is infrequent polling of market rates. All buy/sell actions performed use this exchange, as it is an internal component used by the `BinanceWebSocketExchange`.

#### BinanceWebSocketExchange

Both realtime and polling autotraders can make use of this exchange.

If there is a web socket disconnect the trader will make a number of attempts to reconnect with an exponential back-off for retries. The default retry attempts is set to 5. Afterwhich a failure will force the traders to shutdown. Restarting them will then need to be handled manually. 

### Strategies

| Strategy  | Description  | Trader Compatibility |  
|---|---|---|
|MACDStrategy   | MACD implementation, currently only looking at signal crossover  | Polling |  
|NewStrategy   | Always buy, sell if percent increase target is reached, and buy again on the next run  | Polling, Realtime|  
|TrailingStopLossStrategy   | Follow the market up, and sell if it drops by a specified percent from the high  | Polling, Realtime |
|CrashStrategy   | Buy-in if rate has dropped by a specified percent from the previous rate  | Polling |
|BasicStrategy   | Sell if rate increases by a specified percent, buy if it drops again from the last sell  | Polling, Realtime |
|OneShotStrategy   | Sell if rate increases by a specified percent, and shutdown the trader afterwards  | Polling, Realtime |
