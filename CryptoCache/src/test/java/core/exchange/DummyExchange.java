/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.exchange;

import java.math.BigDecimal;
import java.util.List;

import com.binance.api.client.domain.market.Candlestick;

import core.CandlestickPeriod;
import core.ICTExchangeResponse;
import core.autotrader.MachineState;
import core.exchange.iface.IExchange;
import core.marketdata.IMarketData;
import core.marketdata.Rate;

public class DummyExchange implements IExchange {

	private static final String NAME = "DummyExchange";
	
	private int dayCount = 0;
	private IMarketData marketData; 
	private List<Rate> rates;
	
	public DummyExchange(IMarketData marketData) {
		this.marketData = marketData;
		this.rates = marketData.getRates();
	}
	
	
	@Override
	public BigDecimal getRate(String symbol, MachineState buySellState) {
		if (dayCount < rates.size()) {
			return rates.get(dayCount++).getRate();
		}
		dayCount = 0;
		throw new RuntimeException("End of MarketData");
	}

	@Override
	public BigDecimal getAccountBalance(String ccy) {
		// TODO Auto-generated method stub
		return BigDecimal.ZERO;
	}

	@Override
	public ICTExchangeResponse buy(String symbol, BigDecimal quantity) {
		return null;
	}

	@Override
	public ICTExchangeResponse sell(String symbol, BigDecimal quantity) {
		return null;
	}


	@Override
	public String getName() {
		return NAME;
	}


	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<Candlestick> getCandlesticks(String symbol, int limit, CandlestickPeriod interval) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public BigDecimal getFee() {
		// TODO Auto-generated method stub
		return null;
	}

}
