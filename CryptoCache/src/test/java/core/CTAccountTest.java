/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class CTAccountTest {

	@Test
	void CTAccountPrimaryDepositTest() {
		BigDecimal toDeposit = new BigDecimal("60.5");
		CTAccount account = new CTAccount("Test Account", "BTC", "USDT", "100", "2000");
		account.depositPrimaryCcy(toDeposit);
		BigDecimal primaryBalance = account.getPrimaryBalance();
		
		assertEquals(new BigDecimal("160.5"), primaryBalance);
	}
	
	
	@Test
	void CTAccountSecondaryDepositTest() {
		BigDecimal toDeposit = new BigDecimal("60.5");
		CTAccount account = new CTAccount("Test Account", "BTC", "USDT", "100", "2000");
		account.depositSecondaryCcy(toDeposit);
		BigDecimal secondaryBalance = account.getSecondaryBalance();
		
		assertEquals(new BigDecimal("2060.5"), secondaryBalance);	
	}
	
	
	@Test
	void CTAccountPrimaryWithdrawTest() {
		BigDecimal toWithdraw = new BigDecimal("60.5");
		CTAccount account = new CTAccount("Test Account", "BTC", "USDT", "100", "2000");
		BigDecimal amount = account.withdrawPrimaryCcy(toWithdraw);
		BigDecimal ramaining = account.getPrimaryBalance();
		
		assertEquals(new BigDecimal("39.5"), ramaining);
		assertEquals(new BigDecimal("60.5"), amount);
	}
	
	@Test
	void CTAccountSecondaryWithdrawTest() {
		BigDecimal toWithdraw = new BigDecimal("60.5");
		CTAccount account = new CTAccount("Test Account", "BTC", "USDT", "100", "2000");
		BigDecimal amount = account.withdrawSecondaryCcy(toWithdraw);
		BigDecimal ramaining = account.getSecondaryBalance();
		
		assertEquals(new BigDecimal("1939.5"), ramaining);
		assertEquals(new BigDecimal("60.5"), amount);
	}
	
	@Test
	void CTAccountPrimaryExceedLimitWithdraw() {
		BigDecimal toWithdraw = new BigDecimal("120");
		CTAccount account = new CTAccount("Test Account", "BTC", "USDT", "100", "2000");
		BigDecimal amount = account.withdrawPrimaryCcy(toWithdraw);
		BigDecimal ramaining = account.getPrimaryBalance();
		
		assertEquals(new BigDecimal("0"), ramaining);
		assertEquals(new BigDecimal("100"), amount);
	}
	
	
	@Test
	void CTAccountSecondaryExceedLimitWithdraw() {
		BigDecimal toWithdraw = new BigDecimal("3000");
		CTAccount account = new CTAccount("Test Account", "BTC", "USDT", "100", "2000");
		BigDecimal amount = account.withdrawSecondaryCcy(toWithdraw);
		BigDecimal ramaining = account.getSecondaryBalance();
		
		assertEquals(new BigDecimal("0"), ramaining);
		assertEquals(new BigDecimal("2000"), amount);
	}

}
