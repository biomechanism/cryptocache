/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.emailer;

import core.emailer.iface.IEmailData;

public class EmailData implements IEmailData {
	private final String subject;
	private final String body;
	
	public EmailData(String subject, String body) {
		this.subject = subject;
		this.body = body;
	}
	
	@Override
	public String getSubject() {
		return subject;
	}
	@Override
	public String getBody() {
		return body;
	}
	
}
