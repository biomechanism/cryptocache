/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.emailer;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Objects;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Emailer {
	
	private static final Logger logger = LoggerFactory.getLogger(Emailer.class);

	private Session session;
	private boolean isActive;
	private String emailUser;
	
	
	public Emailer(Properties props) {
		init(props);
	}
	
	private void init(Properties props) {
		
		this.emailUser = props.getProperty("email.user");
		Objects.nonNull(emailUser);
		String password = props.getProperty("email.password");
		Objects.nonNull(password);
		String active = props.getProperty("email.active");
		Objects.nonNull(active);
		
		this.isActive = Boolean.valueOf(active);
		
		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Emailer.this.emailUser, password);
			}
		};
		
		
		session = Session.getInstance(props, auth);
	}

	
	
	
	public boolean isActive() {
		return isActive;
	}
	
	
	
	public void sendEmail(String toEmail, String subject, String body) {
		
		if(!isActive) {
			return;
		}
		
		try {
			MimeMessage msg = new MimeMessage(session);
			
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress(this.emailUser, "NoReply-JD"));
			msg.setReplyTo(InternetAddress.parse(this.emailUser, false));
			msg.setSubject(subject, "UTF-8");
			msg.setText(body, "UTF-8");

			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			Transport.send(msg);

			logger.debug("EMail Sent Successfully!!");
			
		} catch (UnsupportedEncodingException | MessagingException e) {
			logger.error("Failed to send email", e);
		}
	}
	
	
	
}
