/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

public enum CandlestickPeriod {
	ONE_MINUTE,
	THREE_MINUTE,
	FIVE_MINUTE,
	FIFTEEN_MINUTE,
	THIRTY_MINUTE,
	ONE_HOUR,
	TWO_HOUR,
	FOUR_HOUR,
	SIX_HOUR,
	EIGHT_HOUR,
	TWELVE_HOUR,
	ONE_DAY,
	THREE_DAY,
	ONE_WEEK,
	ONE_MONTH;

	public static CandlestickPeriod getCandlePeriod(String period) {
		return switch (period) {
		case "3m" -> CandlestickPeriod.THREE_MINUTE;
		case "5m" -> CandlestickPeriod.FIVE_MINUTE;
		case "15m" -> CandlestickPeriod.FIVE_MINUTE;
		case "30m" -> CandlestickPeriod.THIRTY_MINUTE;
		case "1h" -> CandlestickPeriod.ONE_HOUR;
		case "2h" -> CandlestickPeriod.TWO_HOUR;
		case "4h" -> CandlestickPeriod.FOUR_HOUR;
		case "6h" -> CandlestickPeriod.SIX_HOUR;
		case "12h" -> CandlestickPeriod.TWELVE_HOUR;
		case "1d" -> CandlestickPeriod.ONE_DAY;
		case "3d" -> CandlestickPeriod.THREE_DAY;
		case "1w" -> CandlestickPeriod.ONE_WEEK;
		case "1M" -> CandlestickPeriod.ONE_MONTH;
		default -> throw new IllegalArgumentException(period + "period is not supported");
		};

	}
}
