/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import org.decimal4j.util.DoubleRounder;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.ExchangeInfo;
import com.binance.api.client.domain.general.FilterType;
import com.binance.api.client.domain.general.SymbolFilter;
import com.binance.api.client.domain.general.SymbolInfo;
import com.binance.api.client.domain.market.TickerStatistics;

public class CLITrader {
	
	private String USER_HOME = System.getProperty("user.home");
	
	Closeable wsClosable;
	
	public static void main(String[] args) {
		new CLITrader().init(args);
	}

	private void init(String[] args) {

		// IStrategy func = rate -> account -> state -> new NewStrategy().analyse(rate,
		// account, state);

		int amount = 392000;

		Double val = (3.92 - 0.00001) % 0.00001;

		BigDecimal bd = new BigDecimal("3.92");
		BigDecimal step = new BigDecimal("0.00001");
		BigDecimal val2 = bd.subtract(step);
		BigDecimal finalVal = val2.remainder(step);

		System.out.println("ROUNDED: " + DoubleRounder.round(3.9277856, 5));
		System.out.println("ROUNDED2: " + DoubleRounder.round(3.9277856, 5, RoundingMode.DOWN));

		try (BufferedReader apiReader = new BufferedReader(new FileReader(USER_HOME + "/.cryptotrader/.binance/.apiKey"));
				BufferedReader secretReader = new BufferedReader(new FileReader(USER_HOME + "/.cryptotrader/.binance/.secretKey"))) {

			Objects.requireNonNull(apiReader);
			Objects.requireNonNull(secretReader);

			String apiKey = Objects.requireNonNull(apiReader.readLine());
			String secretKey = Objects.requireNonNull(secretReader.readLine());

			BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(apiKey, secretKey);
			BinanceApiRestClient client = factory.newRestClient();

			long start = System.currentTimeMillis();
			TickerStatistics stats = client.get24HrPriceStatistics("ETHBUSD");
			long stop = System.currentTimeMillis();
			System.out.println(stats.getLastPrice());
			System.out.println("CALL TIME: " + (stop - start));

			client.shutdown();

//			BinanceApiCallback<List<AllMarketTickersEvent>> callback = new BinanceApiCallback<List<AllMarketTickersEvent>>() {
//
//				@Override
//				public void onResponse(List<AllMarketTickersEvent> response) {
//					System.out.println("WEBSOCKET EVENT RECEIVED");
//					AllMarketTickersEvent event = response.get(0);
//					System.out.println(event);
//					try {
//						CLITrader.this.wsClosable.close();
//						response = null;
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					
//				}
//				
//				
//			    @Override
//			    public void onFailure(final Throwable cause) {
//			        System.err.println("Web socket failed");
//			        cause.printStackTrace(System.err);
//			    }
//				
//			};
//			
//			
//			BinanceApiWebSocketClient webSocketClient = factory.newWebSocketClient();
//			wsClosable = webSocketClient.onAllMarketTickersEvent(callback);

			ExchangeInfo exchangeInfo = client.getExchangeInfo();

			SymbolInfo symbolInfo = exchangeInfo.getSymbolInfo("LTCBUSD");
			SymbolFilter priceFilter = symbolInfo.getSymbolFilter(FilterType.LOT_SIZE);
//			
			System.out.println(priceFilter);

			client.shutdown();
//			
//			client.ping();
//			
//			symbolInfo = exchangeInfo.getSymbolInfo("ETHBTC");
//			priceFilter = symbolInfo.getSymbolFilter(FilterType.LOT_SIZE);
//			
//			long serverTime = client.getServerTime();
//			System.out.println(serverTime);
////			
////		client.
////		client.getAccount(4000L, System.currentTimeMillis());
//		com.binance.api.client.domain.account.Account account = client.getAccount();
//		AssetBalance ethBalance = account.getAssetBalance("ETH");
//		System.out.println("BAL: " + ethBalance.getFree());
//		System.out.println(ethBalance);
//		
//		List<AssetBalance> balances = account.getBalances();
//
//		balances.forEach(System.out::println);
//
//		OrderBook orderBook = client.getOrderBook("ETHTUSD", 10);
//		List<OrderBookEntry> asks = orderBook.getAsks();
//		OrderBookEntry firstAskEntry = asks.get(0);
//		System.out.println(firstAskEntry.getPrice() + " / " + firstAskEntry.getQty());
//
//		String s = "";
//
//
//		List<TickerPrice> prices = client.getAllPrices();
//
//		String price = prices.get(0).getPrice();
//		System.out.println("PRICE: " + price);
//
//		prices.forEach(e -> System.out.println(e));
//		
//		System.out.println("ETHTUSD: " + client.getPrice("ETHTUSD"));
//		System.out.println("TUSDETH: " + client.getPrice("TUSDETH"));
//		System.out.println(client.getPrice("ETHBTC"));

			// callback = null;
			// exchangeInfo = null;
			System.out.println("--- END ---");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void startTrader() {
		
	}
	
}



