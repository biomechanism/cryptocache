package core.strategy;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;

public class AmountStrategy extends AbstractStrategy {

	
	public static final String NAME = "AmountStrategy";
	private static final Logger logger = LoggerFactory.getLogger(AmountStrategy.class);

	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);
		
		return analyse(rate, account, autoTrader);
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		if (state == MachineState.SELL) {
			if (sell(rate, account, autoTrader)) {
				return TradeAction.SELL;
			} else {
				//noActionTaken++;
			}
		} else if (state == MachineState.BUY) {
			if (buy(rate, account, autoTrader)) {
				return TradeAction.BUY;

			} else {
				//noActionTaken++;
			}
		}
			return TradeAction.NONE;
	}
	
	
	
	private boolean shouldSell(BigDecimal coinRate, IAccount account) {
		
		
		BigDecimal trigger = getPreviousBuy().add(getTiggerByAmount(account).add(getFeeByAmount(account)));
		
		if (coinRate.compareTo(trigger) > 0) {
			return true;
		}
		return false;
	}
	
	private boolean shouldBuy(BigDecimal coinRate, IAccount account) {
		
		if (getPreviousBuy().compareTo(BigDecimal.ZERO) == 0) {
			setPreviousBuy(coinRate);
			return true;
		}
		
		
		BigDecimal trigger = getPreviousBuy().subtract(account.getSecondaryBalance().multiply(getTriggerPercent()));
		
		
		if (coinRate.compareTo(trigger) < 0) {
			setPreviousBuy(coinRate);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		if (account.getPrimaryBalance().compareTo(BigDecimal.ZERO) > 0 && shouldSell(rate, account)) {
			try {
				autoTrader.sellMax();
				
				setPreviousBuy(rate);
				autoTrader.setMachineState(MachineState.BUY);
				return true;
			} catch (Exception e) {
				logger.error("Failed to SELL", e);
				//TODO: Also send email
			}
		}

		return false;
		
	}
	
	
	private BigDecimal getFeeByAmount(IAccount account) {
		BigDecimal fee = getFee();
		return account.getPrimaryBalance().multiply(getPreviousBuy()).multiply(fee);
	}
	
	private BigDecimal getTiggerByAmount(IAccount account) {
		BigDecimal triggerPercent = getTriggerPercent();
		BigDecimal triggerAmaount = account.getPrimaryBalance().multiply(triggerPercent);
		return triggerAmaount;
	}
	
	
	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		
//		if (account.getSecondaryBalance().compareTo(rate) > 0 && shouldBuy(rate)) {
		if (shouldBuy(rate, account)) {	
			try {
				autoTrader.buyMax();
				autoTrader.setMachineState(MachineState.SELL);
				return true;
			} catch (Exception e) {
				logger.error("Failed to BUY", e);
				//TODO: Also send email
			}
		}
		return false;
	}


	@Override
	public String getName() {
		return NAME;
	}


}
