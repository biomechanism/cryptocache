/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy.iface;

import java.math.BigDecimal;
import java.util.Optional;

import core.CTAttributes;
import core.IAccount;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;
import core.strategy.TradeAction;

//extends Function<BigDecimal, Function<IAccount, Function<AutoTrader.State, TradeAction>>> {

public interface IStrategy { 
	TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader);
	TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader);
	String getName();
	BigDecimal getPreviousBuy();
	void setPreviousBuy(BigDecimal previousBuy);
	BigDecimal getFee();
	CTAttributes getAttributes();
	Optional<String> getAttribute(String name);
	void setAttribute(String name, String value);
	abstract boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader);
	abstract boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader);
	void setFee(BigDecimal fee);
	BigDecimal getTriggerPercent();
	void setTriggerPercent(BigDecimal triggerPresent);
	void setAtttributes(CTAttributes attributes);
	
}
