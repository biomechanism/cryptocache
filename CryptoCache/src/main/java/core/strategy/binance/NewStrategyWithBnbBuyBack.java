/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy.binance;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CTAccount;
import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.PollingAutoTrader;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;
import core.strategy.AbstractStrategy;
import core.strategy.TradeAction;

public class NewStrategyWithBnbBuyBack extends AbstractStrategy {
	
	public static final String NAME = "NewStrategyWithBnbBuyBack";
	private static final Logger logger = LoggerFactory.getLogger(NewStrategyWithBnbBuyBack.class);

	
	private Function<BigDecimal, Function<IAccount, Function<IAutoTrader, Boolean>>> buyFunc = rate -> account -> trader -> {
		return buy(rate, account, trader);
	};
	
	
	
	
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);

		return analyse(rate, account, autoTrader);
		
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		if (state == MachineState.BUY) {
			if (buy(rate, account, autoTrader)) {
				return TradeAction.BUY;
			}
		} else if (state == MachineState.SELL) {
			if (sell(rate, account, autoTrader)) {
				return TradeAction.SELL;
			}
		} 
			
		//logger.info("SKIPPING DAY");
		return TradeAction.NONE;		
	}
	
	
	//FIXME: This will only buy if the balance can at least purchase 1 at full rate.
	//Sould allow buying of partial amounts of less than 1 unit of primary currency.
	//Particularly important as the prices rise. Need to check balance and LOT amount.
	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		if (account.getSecondaryBalance().compareTo(rate) > 0) {
			try {
				autoTrader.buyMax();
				//previousBuy = rate;
				setPreviousBuy(rate);
				autoTrader.setMachineState(MachineState.SELL);
				return true;
			} catch (RuntimeException e) {
				logger.error("Failed to BUY", e);
				//TODO: Also send email
			}
		}
		
		return false;
	}
	
	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		if (account.getPrimaryBalance().compareTo(BigDecimal.ZERO) > 0 && shouldSell(rate, account, autoTrader)) {
			try {
				BigDecimal fees = calcFees(rate, account, autoTrader);
				autoTrader.sellMax();
				autoTrader.setMachineState(MachineState.BUY);
				buyBnb(fees, rate, account, autoTrader);
				return true;
			} catch (RuntimeException e) {
				logger.error("Failed to SELL", e);
				//TODO: Also send email
			}
		}
		
		return false;
	}
	
	
	private boolean shouldSell(BigDecimal coinRate, IAccount account, IAutoTrader autoTrader) {
		
		BigDecimal fees = calcFees(coinRate, account, autoTrader);
		BigDecimal trigger = calcNewTriggerRate(account, fees);
		
		if (coinRate.compareTo(trigger) > 0) {
			return true;
		}
		
		return false;
	}
	
	private BigDecimal calcNewTriggerRate(IAccount account, BigDecimal fees) {
		BigDecimal primBalance = account.getPrimaryBalance();
		BigDecimal targetAmount = primBalance.multiply(getPreviousBuy()).multiply(BigDecimal.ONE.add(getTriggerPercent())).add(fees);
		BigDecimal targetRate = targetAmount.divide(primBalance, 8, RoundingMode.HALF_DOWN);
		return targetRate;
	}


	private BigDecimal calcFees(BigDecimal coinRate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal buyFee = autoTrader.getBuyFee();
		BigDecimal sellFee = calcSelllFee(coinRate, account);
		BigDecimal bnbBuyBackFee = buyFee.add(sellFee).multiply(getFee());
		return buyFee.add(sellFee).add(bnbBuyBackFee);
 	}
	
	
	private BigDecimal calcSelllFee(BigDecimal coinRate, IAccount account) {
			BigDecimal sellFee = account.getPrimaryBalance().multiply(coinRate).multiply(getFee());
			return sellFee;
	}
	
	
	private Optional<BigDecimal> buyBnb(BigDecimal buyBackBalance, BigDecimal currentRate, IAccount account, IAutoTrader autoTrader) {
		String ccy2 = account.getSecondaryCurrency();
		String symbol = "BNB" + ccy2;
		BigDecimal bnbRate = autoTrader.getExchange().getRate(symbol, MachineState.BUY);
		BigDecimal quantity = calcBnbQuantity(bnbRate, buyBackBalance);
		
		//Bit of a hack to ensure we actually try to buy a bit less than is in the account 
		//so there is no failure.
		quantity = quantity.multiply(new BigDecimal("0.999"));
		
		IAccount bnbAccount = createTempBnbAccount(account);
		
		try {
			return autoTrader.buy(quantity, bnbRate, bnbAccount, symbol, buyBackBalance);
		} catch (Exception e) {
			logger.error("Failed to buy back BNB", e);
			return Optional.empty();
		}
	}

	
	private IAccount createTempBnbAccount(IAccount tradingAccount) {
		class BNBAccount extends CTAccount {
			public BNBAccount(IAccount tradingAccount) {
				super("BNBAccount", "BNB", tradingAccount.getSecondaryCcy(), 
						"0", tradingAccount.getSecondaryBalance().toString());
			}
			
			@Override
			public BigDecimal withdrawSecondaryCcy(BigDecimal amount) {
				return super.withdrawSecondaryCcy(amount);
			}
			
			@Override
			public void depositPrimaryCcy(BigDecimal amount) {
				//Prevent call of parent method
			}
		}
		
		return new BNBAccount(tradingAccount);
	}
	
	
	private BigDecimal calcBnbQuantity(BigDecimal rate, BigDecimal balance) {
		return balance.divide(rate, 8, RoundingMode.HALF_DOWN);
	}


	@Override
	public String getName() {
		return NAME;
	}





}
