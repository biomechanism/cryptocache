/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy.binance;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.api.client.domain.market.Candlestick;

import core.CandlestickPeriod;
import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;
import core.strategy.AbstractMACDStrategy;
import core.strategy.NewStrategy;
import core.strategy.TradeAction;

/*
 * This strategy is currently dependent on the BinanceExchange as it makes use of one
 * of the Binance library classes. 
 * 
 * TODO: Eventually it will be better to provide a custom candle
 * class and over the mapping done from each exchange class.
 * 
 */
public class CustomMACDStrategy extends AbstractMACDStrategy {
	
	private static final Logger logger = LoggerFactory.getLogger(NewStrategy.class);
	public static final String NAME = "CustomMACDStrategy";
	
	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		
		CandlestickPeriod period = getCandlePeriod();
		int candleLimit = getCandleLimit();
		List<Candlestick> candles = marketDataProvider.getCandlesticks(symbol, candleLimit, period);
		
		Candlestick candle = candles.get(candles.size()-1);
		String price = candle.getClose().trim();
		BigDecimal currentClosePrice = new BigDecimal(price);
		autoTrader.setCurrentRate(currentClosePrice);
		
		int shortEMA = getShortEMA();
		int longEMA = getLongEMA();
		int signalEMA = getSignalMA();
		
		BigDecimal macd = marketDataProvider.getMACD(candles, shortEMA, longEMA);
		BigDecimal signal = marketDataProvider.getSignal(candles, shortEMA, longEMA, signalEMA);
		
		if (isSignalCrossed(signal, macd)) {
			if (state == MachineState.BUY && position.equals("ABOVE")) {
				logger.info("BUYING @ {}", currentClosePrice);
				buy(currentClosePrice, account, autoTrader);
			} else if (state == MachineState.SELL && position.equals("BELOW")) {
				
				BigDecimal pb = getPreviousBuy();
				pb = pb.add(pb.multiply(getFee()).multiply(BigDecimal.valueOf(2)));
				pb = pb.add(currentClosePrice.multiply(getProfitTrigger()));
				if (currentClosePrice.compareTo(pb) < 0) {
					return TradeAction.NONE;
				}
				
				logger.info("SELLING @ {}", currentClosePrice);
				sell(currentClosePrice, account, autoTrader);
			}
		}
		
		return TradeAction.NONE;
	}

	//TODO: Maybe also moved to cached version in the abstract class
	private BigDecimal getProfitTrigger() {
		Optional<String> profitTrigger = getAttribute(PROFIT_PERCENT_TRIGGER);
		if (profitTrigger.isPresent()) {
			return new BigDecimal(profitTrigger.get());
		}
		
		throw new IllegalArgumentException("Attribute ProfitPercentTrigger has not been set");
	}

	
	
	
	@Override
	public String getName() {
		return NAME;
	}


}
