/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy.binance;

import java.math.BigDecimal;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;
import core.strategy.AbstractStrategy;
import core.strategy.TradeAction;
import core.strategy.TrailingStopLossStrategy;

public class FUFDStrategy extends AbstractStrategy {
	private static final Logger logger = LoggerFactory.getLogger(TrailingStopLossStrategy.class);
	public static final String NAME = "FUFDStrategy";
	public static final String STOP_LOSS_TRIGGER = "StopLossTrigger";
	public static final String BUYIN_TRIGGER = "BuyInTrigger";

	private BigDecimal previousRate = BigDecimal.ZERO;
	private BigDecimal highRate = BigDecimal.ZERO;
	private BigDecimal lowRate = BigDecimal.ZERO;
	
	
	public FUFDStrategy() {
		setAttribute(STOP_LOSS_TRIGGER, "0.03");
		setAttribute(BUYIN_TRIGGER, "0.01");
	}
	
	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);

		return analyse(rate, account, autoTrader);
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		
		BigDecimal currentRate = rate;
		
		if (previousRate.compareTo(BigDecimal.ZERO) == 0) {
			previousRate = rate;
			return TradeAction.NONE;
		}
		
		if (currentRate.compareTo(highRate) > 0) {
			highRate = currentRate;
		}
		
		if (currentRate.compareTo(lowRate) < 0) {
			lowRate = currentRate;
		}
		
		
		if (highRate.compareTo(currentRate.multiply(getStopLossTrigger())) > 0 && state == MachineState.SELL) {
			try {
				if (sell(currentRate, account, autoTrader)) {
					logger.debug("(SELLING) - Current Rate: {}, High Rate: {}, Fee: {}", currentRate, highRate, getFee());
					autoTrader.sellMax();
					previousRate = currentRate;
					resetHiLowRates(currentRate);
					autoTrader.setMachineState(MachineState.BUY);
					return TradeAction.SELL;
				}
			} catch (Exception e) {
				logger.error("Failed to SELL", e);
			}
		} else if (state == MachineState.BUY) {
			if (lowRate.compareTo(BigDecimal.ZERO) != 0) {
				if (currentRate.compareTo(lowRate.multiply(getStatBuyInTrigger())) > 0 ) {
					try {
						if (buy(currentRate, account, autoTrader)) {
							BigDecimal buyInTrigger = lowRate.multiply(getStatBuyInTrigger()).add(getFee());
							logger.debug("(BUYING) - Current Rate: {}, Low Rate: {}, Buy In Trigger {}, Fee: {}", currentRate, lowRate, buyInTrigger, getFee());
							autoTrader.buyMax();
							previousRate = currentRate;
							resetHighRate(currentRate);
							autoTrader.setMachineState(MachineState.SELL);
							return TradeAction.BUY;
						}
					} catch (Exception e) {
						logger.error("Failed to BUY", e);
					}
				}
			} else {
				if (buy(currentRate, account, autoTrader)) {
					autoTrader.buyMax();
					previousRate = currentRate;
					resetHighRate(currentRate);
					autoTrader.setMachineState(MachineState.SELL);
					return TradeAction.BUY;
				}
			}
			
		}
		
		
		return TradeAction.NONE;		
	}



	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal secondaryBalance = account.getSecondaryBalance();
		if (secondaryBalance.compareTo(BigDecimal.ZERO) > 0) {
			return true;
		}
		return false;
	}

	
	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal primaryBalance = account.getPrimaryBalance();
		
		if (primaryBalance.compareTo(BigDecimal.ZERO) > 0) { 
			return true;
		}
		return false;
	}
	

	
	private void resetHiLowRates(BigDecimal currentRate) {
		resetHighRate(currentRate);
		resetLowRate(currentRate);
	}
	
	private void resetHighRate(BigDecimal currentRate) {
		highRate = currentRate;
	}
	
	
	private void resetLowRate(BigDecimal currentRate) {
		lowRate = currentRate;
	}
	
	
	private BigDecimal getStopLossTrigger() {
		Optional<String> value = getAttribute(STOP_LOSS_TRIGGER);
		if (value.isPresent()) {
			BigDecimal stopLoss =  new BigDecimal(value.get());
			return stopLoss.add(BigDecimal.ONE);
		}
		return BigDecimal.ZERO;
	}
	
	
	private BigDecimal getStatBuyInTrigger() {
		Optional<String> value = getAttribute(BUYIN_TRIGGER);
		if (value.isPresent()) {
			BigDecimal buyIn = new BigDecimal(value.get());
			return buyIn.add(BigDecimal.ONE);
		}
		return BigDecimal.ZERO;
	}
	
	
	@Override
	public void setAttribute(String name, String value) {
		switch (name) {
		case STOP_LOSS_TRIGGER:
			super.attributes.setAttribute(STOP_LOSS_TRIGGER, value);
			break;
		case BUYIN_TRIGGER:
			super.attributes.setAttribute(BUYIN_TRIGGER, value);
			break;
		default:
			super.setAttribute(name, value);
		}
	}
	
	
	@Override
	public String getName() {
		return NAME;
	}



}
