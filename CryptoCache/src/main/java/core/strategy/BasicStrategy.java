/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;


//FIXME: Refactor and fix
public class BasicStrategy extends AbstractStrategy {
	
	public static final String NAME = "BasicStrategy";
	
	private static final Logger logger = LoggerFactory.getLogger(BasicStrategy.class);
	boolean forceBuy = false;
	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);
		
		return analyse(rate, account, autoTrader);
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		if (state == MachineState.SELL) {
			if (sell(rate, account, autoTrader)) {
				return TradeAction.SELL;
			} else {
				//noActionTaken++;
			}
		} else if (state == MachineState.BUY) {
			if (buy(rate, account, autoTrader)) {
				return TradeAction.BUY;

			} else {
				//noActionTaken++;
			}
		}
			return TradeAction.NONE;
	}
	
	
	
	private boolean shouldSell(BigDecimal coinRate) {
		//double trigger = lastBuyRate * (1 + triggerPercent + fee);
		BigDecimal trigger = getPreviousBuy().multiply(BigDecimal.ONE.add(getTriggerPercent().add(getFee())));
		
		if (coinRate.compareTo(trigger) > 0) {
			return true;
		}
		return false;
	}
	
	private boolean shouldBuy(BigDecimal coinRate) {
		
		if (getPreviousBuy().compareTo(BigDecimal.ZERO) == 0) {
			setPreviousBuy(coinRate);
			return true;
		}
		
		BigDecimal trigger = getPreviousBuy().subtract(getPreviousBuy().multiply(getTriggerPercent()));
		
		if (coinRate.compareTo(trigger) < 0) {
			setPreviousBuy(coinRate);
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		if (account.getPrimaryBalance().compareTo(BigDecimal.ZERO) > 0 && shouldSell(rate)) {
			try {
				autoTrader.sellMax();
				
				setPreviousBuy(rate);
				autoTrader.setMachineState(MachineState.BUY);
				return true;
			} catch (Exception e) {
				logger.error("Failed to SELL", e);
				//TODO: Also send email
			}
		}

		return false;
		
	}
	
	
	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		if (shouldBuy(rate)) {	
			try {
				autoTrader.buyMax();
				autoTrader.setMachineState(MachineState.SELL);
				return true;
			} catch (Exception e) {
				logger.error("Failed to BUY", e);
				//TODO: Also send email
			}
		}
		return false;
	}


	@Override
	public String getName() {
		return NAME;
	}








}
