/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;

public class NewStrategyDelayed extends AbstractStrategy {
	
	public static final String NAME = "NewStrategyDelayed";
	private static final Logger logger = LoggerFactory.getLogger(NewStrategyDelayed.class);

	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {

		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);

		logger.info("[{}] Checking market, {} rate = {} {}", getName(), account.getPrimaryCcy(), rate, account.getSecondaryCcy());
		
		return analyse(rate, account, autoTrader);

	}
	
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		if (state == MachineState.BUY) {
			if (buy(rate, account, autoTrader)) {
				return TradeAction.BUY;
			}
		} else if (state == MachineState.SELL) {
			if (sell(rate, account, autoTrader)) {
				return TradeAction.SELL;
			}
		}

		return TradeAction.NONE;	
	}
	
	
	//FIXME: This will only buy if the balance can at least purchase 1 at full rate.
	//Sould allow buying of partial amounts of less than 1 unit of primary currency.
	//Particularly important as the prices rise. Need to check balance and LOT amount.
	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {

		try {
			autoTrader.buyMax();
			setPreviousBuy(rate);
			autoTrader.setMachineState(MachineState.SELL);
			return true;
		} catch (RuntimeException e) {
			logger.error("Failed to BUY", e);
			// TODO: Also send email
		}

		return false;
	}
	
	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		if (account.getPrimaryBalance().compareTo(BigDecimal.ZERO) > 0 && shouldSell(rate, account)) {
			try {
				autoTrader.sellMax();
				try {
					Thread.currentThread().sleep(1000 * 60 * 5);
				} catch (InterruptedException e) {
					logger.error("Thread inturrepted", e);
				}
				autoTrader.setMachineState(MachineState.BUY);
				return true;
			} catch (RuntimeException e) {
				logger.error("Failed to SELL", e);
			}
		}
		
		return false;
	}
	
	
	private boolean shouldSell(BigDecimal coinRate, IAccount account) {
		
		BigDecimal trigger = getPreviousBuy().add(getTiggerByAmount(account).add(getFeeByAmount(account)));
		
		if (coinRate.compareTo(trigger) > 0) {
			return true;
		}
		
		return false;
	}
	
	
	
	private BigDecimal getFeeByAmount(IAccount account) {
		BigDecimal fee = getFee();
		return account.getPrimaryBalance().multiply(getPreviousBuy()).multiply(fee);
	}
	
	private BigDecimal getTiggerByAmount(IAccount account) {
		BigDecimal triggerPercent = getTriggerPercent();
		BigDecimal triggerAmaount = account.getPrimaryBalance().multiply(triggerPercent);
		return triggerAmaount;
	}
	


	@Override
	public String getName() {
		return NAME;
	}





}
