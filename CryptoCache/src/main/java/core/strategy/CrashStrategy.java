/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;

public class CrashStrategy extends AbstractStrategy {

	private static final Logger logger = LoggerFactory.getLogger(CrashStrategy.class);
	
	private static final String CRASH_TRIGGER = "CRASH_TRIGGER";
	
	public static final String NAME = "CrashStrategy";
	private BigDecimal previousRate = BigDecimal.ZERO;
	
	
	public CrashStrategy() {
		setAttribute(PREVIOUS_BUY_RATE, "0");
		setAttribute(CRASH_TRIGGER, "1.2");
		setAttribute(AbstractStrategy.PROFIT_PERCENT_TRIGGER, "2");
	}
	
	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);
		autoTrader.setCurrentRate(rate);
		
		return runAnalyse(rate, account, autoTrader);
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalyse(rate, account, autoTrader);
	}


	private TradeAction runAnalyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		
		MachineState state = autoTrader.getMachineState();
		
		BigDecimal currentRate = rate;
		
		if (previousRate.compareTo(BigDecimal.ZERO) == 0) {
			previousRate = rate;
			return TradeAction.NONE;
		}

		// previousRate > currentRate * 1.5 && state == State.BUY
		if (previousRate.compareTo(currentRate.multiply(getCrashTrigger())) > 0 && state == MachineState.BUY) {

			if (buy(currentRate, account, autoTrader)) {
				try {
					autoTrader.buyMax();
					setPreviousRate(currentRate);
					setPreviousBuy(currentRate);
					autoTrader.setMachineState(MachineState.SELL);
					return TradeAction.BUY;
				} catch (RuntimeException e) {
					logger.error("Failed to BUY", e);
				}
			}
			
		}
		
		//currentRate > previousBuy * 2 && state == State.SELL
		if (currentRate.compareTo(getPreviousBuy().multiply(getTriggerPercent())) > 0 && state == MachineState.SELL) {
			if (sell(currentRate, account, autoTrader)) {
				try {
					autoTrader.sellMax();
					previousRate = currentRate;
					autoTrader.setMachineState(MachineState.BUY);
					return TradeAction.SELL;
				} catch (Exception e) {
					logger.error("Failed to SELL", e);
				}
			}
		}
		
		previousRate = currentRate;
		
		return TradeAction.NONE;
	}
	
	
	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal secondaryBalance = account.getSecondaryBalance();
		if (secondaryBalance.compareTo(rate) > 0) {
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal primaryBalance = account.getPrimaryBalance();
		
		if (primaryBalance.compareTo(BigDecimal.ZERO) > 0) { 
			return true;
		}
		return false;
	}


	public BigDecimal getPreviousRate() {
		return previousRate;
	}


	public void setPreviousRate(BigDecimal previousRate) {
		setAttribute(PREVIOUS_BUY_RATE, previousRate.toString());
		this.previousRate = previousRate;
	}

	
	@Override
	public void setAttribute(String name, String value) {
		switch (name) {
		case CRASH_TRIGGER:
			super.attributes.setAttribute(CRASH_TRIGGER, value);
			break;
		default:
			super.setAttribute(name, value);
		}
	}
	
	
	private BigDecimal getCrashTrigger() {
		Optional<String> value = getAttribute(CRASH_TRIGGER);
		if (value.isPresent()) {
			return new BigDecimal(value.get());
		}
		return BigDecimal.ZERO;
	}
	

	@Override
	public String getName() {
		return NAME;
	}





}
