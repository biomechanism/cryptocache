/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;


/*
 * TODO: Make polling autotraders support TERMINATE state.
 * Curently only usable with RealtimeAutoTrader and NewListingAutoTrader, as MachineState.TERMINATE is 
 * not currently supported by polling autotraders.
 */
public class OneShotStrategy extends AbstractStrategy {
	private static final Logger logger = LoggerFactory.getLogger(OneShotStrategy.class);
	public static final String NAME = "OneShotStrategy";
	
	public OneShotStrategy() {
		setAttribute(AbstractStrategy.PROFIT_PERCENT_TRIGGER, "0.4");
	}
	
	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);
		
		return analyse(rate, account, autoTrader);

	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		if (state == MachineState.BUY) {
			if (buy(rate, account, autoTrader)) {
				return TradeAction.BUY;
			}
		} else if (state == MachineState.SELL) {
			if (sell(rate, account, autoTrader)) {
				autoTrader.setMachineState(MachineState.TERMINATE);
				return TradeAction.SELL;
			}
		} 
			
		return TradeAction.NONE;		
	}



	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		if (account.getSecondaryBalance().compareTo(rate) > 0) {
			try {
				autoTrader.buyMax();
				setPreviousBuy(rate);
				autoTrader.setMachineState(MachineState.SELL);
				return true;
			} catch (RuntimeException e) {
				logger.error("Failed to BUY", e);
				//TODO: Also send email
			}
		}
		return false;
	}

	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		if (account.getPrimaryBalance().compareTo(BigDecimal.ZERO) > 0 && shouldSell(rate)) {
			try {
				autoTrader.sellMax();
				autoTrader.setMachineState(MachineState.BUY);
				return true;
			} catch (RuntimeException e) {
				logger.error("Failed to SELL", e);
				//TODO: Also send email
			}
		}
		
		return false;
	}
	
	
	private boolean shouldSell(BigDecimal coinRate) {
		
		//trigger = previousBuy * (1 + triggerPresent + fee);
		BigDecimal trigger = getPreviousBuy().multiply((BigDecimal.ONE.add(getTriggerPercent().add(getFee()))));
		
		if (coinRate.compareTo(trigger) > 0) {
			return true;
		}
		
		return false;
	}
	

	@Override
	public String getName() {
		return NAME;
	}

	
}
