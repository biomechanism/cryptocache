/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;
import java.util.Optional;

import core.CTAttributes;
import core.IAccount;
import core.autotrader.iface.IAutoTrader;
import core.strategy.iface.IStrategy;

public abstract class AbstractStrategy implements IStrategy {

	protected CTAttributes attributes = new CTAttributes();

	public static final String PROFIT_PERCENT_TRIGGER = "ProfitPercentTrigger";
	public static final String TRADE_FEE = "TradeFee";
	public static final String PREVIOUS_BUY_RATE = "PreviousBuyRate";

	private BigDecimal triggerPercent = new BigDecimal("0.05");
	private BigDecimal previousBuy = new BigDecimal("0");
	private BigDecimal cachedTradeFee = null;

	public AbstractStrategy() {
		attributes.setAttribute(PREVIOUS_BUY_RATE, "0");
		attributes.setAttribute(PROFIT_PERCENT_TRIGGER, "0.05");
	}

	@Override
	public BigDecimal getPreviousBuy() {
		return previousBuy;
	}

	@Override
	public void setPreviousBuy(BigDecimal previousBuy) {
		this.previousBuy = previousBuy;
		CTAttributes attribs = getAttributes();
		attribs.setAttribute(AbstractStrategy.PREVIOUS_BUY_RATE, previousBuy.toString());
		this.attributes = attribs;
	}

	@Override
	public BigDecimal getFee() {
		if (cachedTradeFee != null) {
			return cachedTradeFee;
		}
		Optional<String> fee = getAttribute(TRADE_FEE);
		if (fee.isPresent()) {
			cachedTradeFee = new BigDecimal(fee.get());
			return cachedTradeFee;
		}
		
		throw new IllegalArgumentException("Attribute TradeFee has not been set");
		
	}
	
	@Override
	public CTAttributes getAttributes() {
		return new CTAttributes(this.attributes);
	}

	@Override
	public void setAtttributes(CTAttributes attributes) {
		this.attributes = attributes;
	}

	@Override
	public Optional<String> getAttribute(String name) {
		return attributes.getAttribute(name);
	}

	@Override
	public void setAttribute(String name, String value) {
		switch (name) {
		case TRADE_FEE:
			setFee(new BigDecimal(value));
			attributes.setAttribute(name, value);
			break;
		case PREVIOUS_BUY_RATE:
			attributes.setAttribute(name, value);
			setPreviousBuy(new BigDecimal(value));
			break;
		case PROFIT_PERCENT_TRIGGER:
			attributes.setAttribute(name, value);
			setTriggerPercent(new BigDecimal(value));
			break;
		default:
			throw new IllegalArgumentException("Invalid attribute " + name);
		}
	}

	@Override
	public void setFee(BigDecimal fee) {
		this.cachedTradeFee = fee;
	}

	@Override
	public BigDecimal getTriggerPercent() {
		return triggerPercent;
	}

	@Override
	public void setTriggerPercent(BigDecimal triggerPresent) {
		this.triggerPercent = triggerPresent;
	}
	
	
	@Override
	public String toString() {
		String EOL = System.lineSeparator();
		StringBuilder buffer = new StringBuilder();
		buffer.append("Strategy = ").append(getName()).append(EOL);
		return buffer.toString();

	}
	
}
