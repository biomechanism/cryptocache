package core.strategy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;

public class CostAveragingStrategy extends AbstractStrategy {
	
	private static final Logger logger = LoggerFactory.getLogger(CostAveragingStrategy.class);
	
	private long runId;
	
	public static final String NAME = "CostAveragingStrategy";
	private static final BigDecimal DIVISOR = new BigDecimal("8");
	
	private IAutoTrader autoTrader;
	private Deque<TradeAllocation> buyQueue = new ArrayDeque<>();
	private Deque<TradeAllocation> sellQueue = new ArrayDeque<>();
	private Deque<TradeAllocation> sellExecQueue = new ArrayDeque<>();
	
	//private int bought = 0;
	

	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);
		autoTrader.setCurrentRate(rate);
		this.autoTrader = autoTrader;
		return analyse(rate, account, autoTrader);
	}

	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		logger.info("[{}] Rate {}", this.autoTrader.getName(), rate);
		
		this.runId = System.nanoTime();
		if (isInitialRun()) {
			doTradeAllocation(account, rate);
		}
		
		
		doSell(rate, autoTrader);
		
		doBuy(rate, autoTrader);
		

		return TradeAction.NONE;

	}



	private void doSell(BigDecimal rate, IAutoTrader autoTrader) {

		if (sellQueue.isEmpty()) {
			return;
		}
		
		int count = 1;
		for (TradeAllocation allocation : sellQueue) {
			
			if (allocation.getRunId() == this.runId) {
				continue;
			}
			
			
			BigDecimal previousBuy = allocation.getPreviousBuyRate();
			
			//double trigger = lastBuyRate * (1 + triggerPercent + fee);
			BigDecimal trigger = previousBuy
					.multiply(BigDecimal.ONE.add(getTriggerPercent().multiply(new BigDecimal(count)).add(getFee())));

			if (rate.compareTo(trigger) > 0) {
				logger.info("Added allocation for sell execution");
				sellExecQueue.add(allocation);
			}
			
			count++;
		}
		
		if (sellExecQueue.isEmpty()) {
			return;
		}
		
		sellQueue.removeAll(sellExecQueue);
		
		for (TradeAllocation allocation : sellExecQueue) {
			BigDecimal qty = allocation.getPrimaryBalance();
			Optional<BigDecimal> executedQty = autoTrader.sell(qty);
			allocation.setSecondaryBalance(executedQty.get());
			
			if (!executedQty.isEmpty()) {
				logger.info("[{}] SELL: Quantity {} @ Rate {}", this.autoTrader.getName(), qty, rate);
				addToBuyQueue(allocation);
			}
			
		}
		
		sellExecQueue.clear();
		
		
	}
	
	
	
	private void addToBuyQueue(TradeAllocation allocation) {
		buyQueue.add(allocation);
	}
	
	
	

	private void doBuy(BigDecimal rate, IAutoTrader autoTrader) {
		
		if (buyQueue.isEmpty()) {
			return;
		}

		if (buyQueue.peek().getRunId() == this.runId) {
			return;
		}
		
		TradeAllocation allocation = buyQueue.pop();
		
		BigDecimal secBal = allocation.getSecondaryBalance(); //divide(DIVISOR, 8, RoundingMode.HALF_DOWN);
		BigDecimal quantity = secBal.divide(rate, 8, RoundingMode.HALF_DOWN);
		
		Optional<BigDecimal> executedQty = autoTrader.buy(quantity);
		allocation.setPrimaryBalance(executedQty.get());
		
		allocation.setPreviousBuyRate(rate);
		
		logger.info("[{}] BUY: Quantity {} @ Rate {}", this.autoTrader.getName(), executedQty, rate);
		
		sellQueue.add(allocation);
		
	}

	private boolean isInitialRun() {
		return buyQueue.isEmpty() && sellQueue.isEmpty();
	}

	

	private void doTradeAllocation(IAccount account, BigDecimal rate) {
		
		String primaryCcy = account.getPrimaryCcy();
		String secondaryCcy = account.getSecondaryCcy();
		BigDecimal primaryBalance = account.getPrimaryBalance();
		BigDecimal secondaryBalance = account.getSecondaryBalance();
		
		if (secondaryBalance.compareTo(BigDecimal.ZERO) > 0) {
			BigDecimal allocationAmount = secondaryBalance.divide(DIVISOR, 8, RoundingMode.HALF_DOWN);
			for (int i = 0; i < DIVISOR.intValue(); i++) {
				TradeAllocation allocation = new TradeAllocation(allocationAmount, primaryCcy, secondaryCcy,
						primaryBalance, allocationAmount, BigDecimal.ZERO, runId);
				buyQueue.add(allocation);
			}

			return;
		} else if (primaryBalance.compareTo(BigDecimal.ZERO) > 0){
			BigDecimal allocationAmount = primaryBalance.divide(DIVISOR, 8, RoundingMode.HALF_DOWN);
			for (int i = 0; i < DIVISOR.intValue(); i++) {
				TradeAllocation allocation = new TradeAllocation(allocationAmount, primaryCcy, secondaryCcy,
						allocationAmount, secondaryBalance, rate, runId);
				sellQueue.add(allocation);
			}
			return;
		}
		
		throw new IllegalStateException("Invalid balance");
		
		
		
	}
	

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
	
	private class TradeAllocation {
		private long runId;
		private String primaryCurrency;
		private String secondaryCurrency;
		private BigDecimal primaryBalance;
		private BigDecimal secondaryBalance;
		private BigDecimal previousBuyRate;
		private BigDecimal allocationAmount;
		
		
		public TradeAllocation(BigDecimal allocationAmount, String primaryCcy, String secondaryCcy, BigDecimal primaryBalance, BigDecimal secondaryBalance, BigDecimal previousBuyRate, long runId) {
			this.allocationAmount = allocationAmount;
			this.primaryCurrency = primaryCcy;
			this.secondaryCurrency = secondaryCcy;
			this.primaryBalance = primaryBalance;
			this.secondaryBalance = secondaryBalance;
			this.previousBuyRate = previousBuyRate;
			this.runId = runId;
		}


		public String getPrimaryCurrency() {
			return primaryCurrency;
		}


		public void setPrimaryCurrency(String primaryCurrency) {
			this.primaryCurrency = primaryCurrency;
		}


		public String getSecondaryCurrency() {
			return secondaryCurrency;
		}


		public void setSecondaryCurrency(String secondaryCurrency) {
			this.secondaryCurrency = secondaryCurrency;
		}


		public BigDecimal getPrimaryBalance() {
			return primaryBalance;
		}


		public void setPrimaryBalance(BigDecimal primaryBalance) {
			this.primaryBalance = primaryBalance;
		}


		public BigDecimal getSecondaryBalance() {
			return secondaryBalance;
		}


		public void setSecondaryBalance(BigDecimal secondaryBalance) {
			this.secondaryBalance = secondaryBalance;
		}


		public BigDecimal getPreviousBuyRate() {
			return previousBuyRate;
		}


		public void setPreviousBuyRate(BigDecimal previousBuyRate) {
			this.previousBuyRate = previousBuyRate;
		}


		public BigDecimal getAllocationAmount() {
			return allocationAmount;
		}


		public void setAllocationAmount(BigDecimal allocationAmount) {
			this.allocationAmount = allocationAmount;
		}


		public long getRunId() {
			return runId;
		}


		public void setRunId(long runId) {
			this.runId = runId;
		}
		
		
		

		
	}

}
