/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import core.strategy.binance.CustomMACDStrategy;
import core.strategy.binance.FUFDStrategy;
import core.strategy.binance.MACDStrategy;
import core.strategy.binance.MACDStrategyZeroLine;
import core.strategy.binance.NewStrategyWithBnbBuyBack;
import core.strategy.iface.IStrategy;

public class StrategyFactory {

	public static IStrategy getStrategy(String name) {
		return switch (name) {
		case NewStrategy.NAME -> new NewStrategy();
		case NewStrategyWithBnbBuyBack.NAME -> new NewStrategyWithBnbBuyBack();
		case BasicStrategy.NAME-> new BasicStrategy();
		case CrashStrategy.NAME -> new CrashStrategy();
		case MaximiseHoldingStrategy.NAME -> new MaximiseHoldingStrategy();
		case OneShotStrategy.NAME -> new OneShotStrategy();
		case TrailingStopLossStrategy.NAME -> new TrailingStopLossStrategy();
		case TrailingStopLossBuyBackStrategy.NAME -> new TrailingStopLossBuyBackStrategy();
		case MACDStrategy.NAME -> new MACDStrategy();
		case CustomMACDStrategy.NAME -> new CustomMACDStrategy();
		case MACDStrategyZeroLine.NAME -> new MACDStrategyZeroLine();
		case CostAveragingStrategy.NAME -> new CostAveragingStrategy();
		case FUFDStrategy.NAME -> new FUFDStrategy();
		case AmountStrategy.NAME -> new AmountStrategy();
		case NewStrategyDelayed.NAME -> new NewStrategyDelayed();
		default -> throw new IllegalArgumentException("Unexpected value: " + name);
		};
	}

}
