/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;

public class TrailingStopLossBuyBackStrategy extends AbstractStrategy {
	
	private static final Logger logger = LoggerFactory.getLogger(TrailingStopLossStrategy.class);
	public static final String NAME = "TrailingStopLossBuyBackStrategy";
	private static final String STOP_LOSS_TRIGGER = "StopLossTrigger";
	private static final String BUY_BACK_TRIGGER = "BuyBackTrigger";
	
	private BigDecimal previousRate = BigDecimal.ZERO;
	private BigDecimal highRate = BigDecimal.ZERO;
	
	
	public TrailingStopLossBuyBackStrategy() {
		setAttribute(STOP_LOSS_TRIGGER, "0.05");
		setAttribute(BUY_BACK_TRIGGER, "0.8");
	}

	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);

		return analyse(rate, account, autoTrader);
	}
	
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		BigDecimal currentRate = rate;
		
		if (previousRate.compareTo(BigDecimal.ZERO) == 0) {
			previousRate = rate;
			return TradeAction.NONE;
		}
		
		if (currentRate.compareTo(highRate) > 0) {
			highRate = currentRate;
		}
		
		if (state == MachineState.BUY) {
			BigDecimal buyBackTrigger = currentRate.multiply(getBuyBackTrigger());
			if (highRate.compareTo(buyBackTrigger) > 0 && state == MachineState.BUY) {
				autoTrader.buyMax();
				previousRate = currentRate;
				highRate = currentRate;
				autoTrader.setMachineState(MachineState.TERMINATE);
				return TradeAction.NONE;
			}
		} else if (state == MachineState.SELL) {
			if (highRate.compareTo(currentRate.multiply(getStopLossTrigger())) > 0 && state == MachineState.SELL) {
				if (sell(currentRate, account, autoTrader)) {
					autoTrader.sellMax();
					previousRate = currentRate;
					autoTrader.setMachineState(MachineState.BUY);
					return TradeAction.SELL;
				}
			}
		}
		
		
		return TradeAction.NONE;
	}



	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal primaryBalance = account.getPrimaryBalance();
		
		if (primaryBalance.compareTo(BigDecimal.ZERO) > 0) { 
			return true;
		}
		return false;
	}

	

	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		BigDecimal secondaryBalance = account.getSecondaryBalance();
		
		if (secondaryBalance.compareTo(BigDecimal.ZERO) > 0) {
			return true;
		}
		
		return false;
	}

	
	private BigDecimal getBuyBackTrigger() {
		BigDecimal stopLossTrigger = getStopLossTrigger();
		Optional<String> value = getAttribute(BUY_BACK_TRIGGER);
		if (value.isPresent()) {
			BigDecimal buyBack =  new BigDecimal(value.get());
			return buyBack.add(stopLossTrigger);
		}
		return BigDecimal.ZERO;
	}
	
	
	private BigDecimal getStopLossTrigger() {
		Optional<String> value = getAttribute(STOP_LOSS_TRIGGER);
		if (value.isPresent()) {
			BigDecimal stopLoss =  new BigDecimal(value.get());
			return stopLoss.add(new BigDecimal("1"));
		}
		return BigDecimal.ZERO;
	}
	
	
	@Override
	public void setAttribute(String name, String value) {
		switch (name) {
		case STOP_LOSS_TRIGGER:
			super.attributes.setAttribute(STOP_LOSS_TRIGGER, value);
			break;
		case BUY_BACK_TRIGGER:
			super.attributes.setAttribute(BUY_BACK_TRIGGER, value);
			break;
		default:
			super.setAttribute(name, value);
		}
	}
	
	
	@Override
	public String getName() {
		return NAME;
	}


	
}
