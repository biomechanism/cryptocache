/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CandlestickPeriod;
import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;

public abstract class AbstractMACDStrategy extends AbstractStrategy {

	
	private static final Logger logger = LoggerFactory.getLogger(NewStrategy.class);
	public static final String NAME = "MACDStrategy";

	private static final String CANDLE_PERIOD = "CandlePeriod";
	private static final String CANDLE_LIMIT = "CandleLimit";
	private static final String LONG_EMA = "LongEMA";
	private static final String SHORT_EMA = "ShortEMA";
	private static final String SIGNAL_EMA = "SignalEMA";
	private static final String CANDLE_PERIOD_VAL = "30m";
	private static final String CANDLE_LIMIT_VAL = "500";
	private static final String SHORT_EMA_VAL = "4";
	private static final String LONG_EMA_VAL = "22";
	private static final String SIGNAL_EMA_VAL = "3";
	
	protected String position;

	public AbstractMACDStrategy() {
		setAttribute(CANDLE_LIMIT, CANDLE_LIMIT_VAL);
		setAttribute(CANDLE_PERIOD, CANDLE_PERIOD_VAL);
		setAttribute(SHORT_EMA, SHORT_EMA_VAL);
		setAttribute(LONG_EMA, LONG_EMA_VAL);
		setAttribute(SIGNAL_EMA, SIGNAL_EMA_VAL);
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		throw new UnsupportedOperationException("THe MACD Strategy does not suport this method call");
	}

	@Override
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		autoTrader.buyMax();
		setPreviousBuy(rate);
		autoTrader.setMachineState(MachineState.SELL);
		return true;
	}

	@Override
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		autoTrader.sellMax();
		autoTrader.setMachineState(MachineState.BUY);
		return true;
	}
	
	
	
	private String calcPosValue(BigDecimal signalVal, BigDecimal macdValue) {
		if (macdValue.compareTo(signalVal) > 0) {
			return "ABOVE";
		} else if (macdValue.compareTo(signalVal) < 0) {
			return "BELOW";
		} 
		
		return "SAME";
	}
	
	
	
	protected boolean isSignalCrossed(BigDecimal signalVal, BigDecimal macdVal) {
		String newPos = calcPosValue(signalVal, macdVal);
		
		if (position == null) {
			if (newPos.endsWith("SAME")) {
				position = "ABOVE";
			} else {
				position = newPos;
			}
			return false;
		}
		
		if (newPos.equals("SAME")) {
			return false;
		}

		if (newPos.equals(position)) {
			return false;
		}

		logger.info("SIGNAL CROSSED - NEW VAL: {}", newPos);
		position = newPos;
		return true;

	}
	
	
	protected int getLongEMA() {
		Optional<String> value = getAttribute(LONG_EMA);
		if (value.isPresent()) {
			return Integer.valueOf(value.get());
		}
		
		throw new IllegalArgumentException("Attribute LongEMA has not been set");
	}
	
	
	protected int getShortEMA() {
		Optional<String> value = getAttribute(SHORT_EMA);
		if (value.isPresent()) {
			return Integer.valueOf(value.get());
		}
		
		throw new IllegalArgumentException("Attribute ShortEMA has not been set");
	}
	
	
	protected int getSignalMA() {
		Optional<String> value = getAttribute(SIGNAL_EMA);
		if (value.isPresent()) {
			return Integer.valueOf(value.get());
		}
		
		throw new IllegalArgumentException("Attribute SignalEMA has not been set");
	}
	
	
	protected int getCandleLimit() {
		Optional<String> value = getAttribute(CANDLE_LIMIT);
		if (value.isPresent()) {
			return Integer.valueOf(value.get());
		}
		
		throw new IllegalArgumentException("Attribute CandleLimit has not been set");
	}
	
	
	protected CandlestickPeriod getCandlePeriod() {
		Optional<String> period = getAttribute(CANDLE_PERIOD);
		if (period.isPresent()) {
			return CandlestickPeriod.getCandlePeriod(period.get());
		}
		
		throw new IllegalArgumentException("Attribute CandlePeriod has not been set");
	}
	
	
	@Override
	public void setAttribute(String name, String value) {
		switch (name) {
		case CANDLE_PERIOD:
			attributes.setAttribute(name, value);
			break;
		case CANDLE_LIMIT:
			attributes.setAttribute(name, value);
			break;
		case SHORT_EMA:
			attributes.setAttribute(name, value);
			break;
		case LONG_EMA:
			attributes.setAttribute(name, value);
			break;
		case SIGNAL_EMA:
			attributes.setAttribute(name, value);
			break;
		default:
			super.setAttribute(name, value);
		}
	}


}
