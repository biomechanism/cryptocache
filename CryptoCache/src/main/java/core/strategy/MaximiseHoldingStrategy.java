/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.strategy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.autotrader.MachineState;
import core.autotrader.PollingAutoTrader;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketDataProvider;

public class MaximiseHoldingStrategy extends AbstractStrategy {

	private static final Logger logger = LoggerFactory.getLogger(MaximiseHoldingStrategy.class);
	
	public static final String NAME = "MaximiseHoldingStrategy";	
	private static final String PREVIOUS_BUY_AMOUNT = "PreviousBuyAmount";
	private static final String INITIAL_BUY = "InitialBuy";
	
	public MaximiseHoldingStrategy() {
		setAttribute(PREVIOUS_BUY_AMOUNT, "0");
		setAttribute(INITIAL_BUY, Boolean.TRUE.toString());
	}
	
	
	@Override
	public TradeAction analyse(IMarketDataProvider marketDataProvider, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		String symbol = account.getTickerSymbol();
		BigDecimal rate = marketDataProvider.getRate(symbol, state);

		return analyse(rate, account, autoTrader);
		
	}
	
	
	@Override
	public TradeAction analyse(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		return runAnalysis(rate, account, autoTrader);
	}
	
	
	private TradeAction runAnalysis(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		MachineState state = autoTrader.getMachineState();
		autoTrader.setCurrentRate(rate);
		
		if (state == MachineState.BUY) {
			if (buy(rate, account, autoTrader)) {
				return TradeAction.BUY;
			}
		} else if (state == MachineState.SELL) {
			if (sell(rate, account, autoTrader)) {
				return TradeAction.SELL;
			}
		} 
		
		return TradeAction.NONE;
	}

	
	public boolean buy(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		
		BigDecimal secBalance = account.getSecondaryBalance();
		BigDecimal triggerAmount = secBalance.divide(rate, 8, RoundingMode.HALF_DOWN);
		
		if (isInitialBuy() || triggerAmount.compareTo(getPreviousBuyAmount().multiply(getTriggerPercent())) > 0) {
			setPreviousBuyAmount(autoTrader.buyMax());
			setPreviousBuy(rate);
			autoTrader.setMachineState(MachineState.SELL);
			return true;
		}
		
		return false;
	}
	
	
	private boolean isInitialBuy() {
		if (getInitialBuy()) {
			setAttribute(INITIAL_BUY, Boolean.FALSE.toString());
			return true;
		}
		return false;
	}

	
	private boolean getInitialBuy() {
		Optional<String> value = getAttribute(INITIAL_BUY);
		if (value.isPresent()) {
			return Boolean.valueOf(value.get());
		}
		return Boolean.FALSE;
	}
	
	
	public boolean sell(BigDecimal rate, IAccount account, IAutoTrader autoTrader) {
		if (account.getPrimaryBalance().compareTo(BigDecimal.ZERO) > 0 && shouldSell(rate)) {
			try {
				autoTrader.sellMax();
				autoTrader.setMachineState(MachineState.BUY);
				return true;
			} catch (RuntimeException e) {
				logger.error("Failed to SELL", e);
				//TODO: Also send email
			}
		}
		
		return false;
	}

	
	private boolean shouldSell(BigDecimal rate) {
		//trigger = previousBuy * (1 + triggerPresent + fee);
		BigDecimal trigger = getPreviousBuy().multiply((BigDecimal.ONE.add(getTriggerPercent().add(getFee()))));
		if (rate.compareTo(trigger) > 0) {
			return true;
		}
		
		return false;
	}

	
	private void setPreviousBuyAmount(Optional<BigDecimal> amount) {
		if (amount.isPresent()) {
			//previousBuyAmount = amount.get();
			setAttribute(PREVIOUS_BUY_AMOUNT, amount.get().toString());
		}
	}
	
	
	private BigDecimal getPreviousBuyAmount() {
		Optional<String> amount = getAttribute(PREVIOUS_BUY_AMOUNT);
		if (amount.isPresent()) {
			return new BigDecimal(amount.get());
		}
		
		return BigDecimal.ZERO;
		
	}
	
	
	@Override
	public void setAttribute(String name, String value) {
		switch (name) {
		case INITIAL_BUY:
			super.attributes.setAttribute(INITIAL_BUY, value);
			break;
		case PREVIOUS_BUY_AMOUNT:
			super.attributes.setAttribute(PREVIOUS_BUY_AMOUNT, value);
			break;
		default:
			super.setAttribute(name, value);
		}
	}

	@Override
	public String getName() {
		return NAME;
	}






}
