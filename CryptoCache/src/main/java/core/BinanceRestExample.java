/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.general.ExchangeInfo;
import com.binance.api.client.domain.general.SymbolFilter;
import com.binance.api.client.domain.market.TickerPrice;

public class BinanceRestExample {

	public static void main(String[] args) {

		String USER_HOME = System.getProperty("user.home");

		try (BufferedReader apiReader = new BufferedReader(new FileReader(USER_HOME + "/.cryptotrader/.binance/.apiKey"));
				BufferedReader secretReader = new BufferedReader(new FileReader(USER_HOME + "/.cryptotrader/.binance/.secretKey"))) {
			
			Objects.requireNonNull(apiReader);
			Objects.requireNonNull(secretReader);
			
			String apiKey = Objects.requireNonNull(apiReader.readLine());
			String secretKey = Objects.requireNonNull(secretReader.readLine());
			
			BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(apiKey, secretKey);
			
			BinanceApiRestClient client = factory.newRestClient();
			String key = client.startUserDataStream();
			
			List<TickerPrice> prices = client.getAllPrices();
			
			
			ExchangeInfo exchangeInfo = client.getExchangeInfo();
			exchangeInfo.getSymbols();
			
			List<SymbolFilter> filters = exchangeInfo.getSymbolInfo("ETHUSDT").getFilters();
			
			client.closeUserDataStream(key);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
