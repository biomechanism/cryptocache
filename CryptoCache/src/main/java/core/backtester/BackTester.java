/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.backtester;

import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CTAccount;
import core.IAccount;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.IMarketData;

public class BackTester {

	private static final Logger logger = LoggerFactory.getLogger(BackTester.class);
	
	private String name;
	private IAccount startingAccount;
	private IAccount account;
	private IAutoTrader autoTrader;
	
	
	public BackTester(String name, IAccount account, IAutoTrader autoTrader) {
		this.startingAccount = new CTAccount(account);
		this.account = account;
		this.autoTrader = autoTrader;
		this.name = name;
	}
	
	
	public BackTester(String name, IAccount account, BiFunction<Double, IAccount, Boolean> strategy) {
		this.startingAccount = new CTAccount(account);
		this.account = account;
		this.name = name;
	}
	
	public boolean runAutoTrader() {
		logger.info("========== {} ==========", name);

		this.autoTrader.start(1, 1, TimeUnit.NANOSECONDS);

		return true;
	}
	
	
	private boolean outputResult(IMarketData marketData) {

//		BigDecimal primaryBalance = account.getPrimaryBalance();
//		String primaryCcy = account.getPrimaryCurrency();
//		
//		BigDecimal secondaryBalance = account.getSecondaryBalance();
//		String secondaryCcy = account.getSecondaryCurrency();
//		
//		logger.info("{} Balance: {}", primaryCcy, primaryBalance);
//		logger.info("{} Balance: {}", secondaryCcy, secondaryBalance);
//		
//		
//		double profit = ((account.getSecondaryBalance() * 100) / startingAccount.getSecondaryBalance()) -100;
//		logger.info("{} Profit: {}", secondaryCcy, profit);
//		
//		if (primaryBalance > 0) {
//			//double profit = ((primaryBalance * 100) / startingAccount.getPrimaryBalance()) -100;
//			int last = marketData.getRates().size();
//			double lastRate = marketData.getRates().get(last-1).getRate();
//			logger.info("Unrealised Balance: {}, Last Rate {}", (primaryBalance * lastRate), lastRate);
//			
//			double unrealisedProfit = ((primaryBalance * lastRate) * 100) / startingAccount.getPrimaryBalance() -100;
//			logger.info("Unrealised Profit: {}", unrealisedProfit);
//		}
//		
		return true;
		
	}
	
	
}
