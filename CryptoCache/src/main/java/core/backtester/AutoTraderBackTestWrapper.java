/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.backtester;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.StartTime;
import core.autotrader.MachineState;
import core.autotrader.iface.IAutoTrader;
import core.backtester.exception.EndOfBacktestMarketData;
import core.exchange.iface.IExchange;
import core.strategy.iface.IStrategy;

public class AutoTraderBackTestWrapper implements IAutoTrader {

	private static final Logger logger = LoggerFactory.getLogger(AutoTraderBackTestWrapper.class);
	
	private boolean started = false;
	private final IAutoTrader autoTrader;
	
	private volatile ScheduledExecutorService executor;
	private volatile ScheduledFuture<?> task;
	
	public AutoTraderBackTestWrapper(IAutoTrader autoTrader) {
		this.autoTrader = autoTrader;
	}
	
	
	@Override
	public void run() {

		
		IAccount tradingAccount = getTradingAccount();
		IExchange exchange = getExchange();
		IStrategy strategy = getStrategy();
		
		try {
			BigDecimal rate = exchange.getRate(tradingAccount.getTickerSymbol(), autoTrader.getMachineState());
			setCurrentRate(rate);
			strategy.analyse(rate, tradingAccount, this);
		} catch (EndOfBacktestMarketData e) {
			stop();
			logger.info("End of Backtester MarketData, shutting down trader {}", this.getName());
		}
	}

	@Override
	public String getName() {
		return autoTrader.getName();
	}

	@Override
	public boolean start() {
		return start(1L, 1L, TimeUnit.MILLISECONDS);
	}
	
	@Override
	public boolean start(long delay, long period, TimeUnit timeUnit) {
		executor = Executors.newScheduledThreadPool(1);
		task = (ScheduledFuture<?>) executor.scheduleAtFixedRate(this, delay, period, timeUnit);
		started = true;
		return true;
	}

	@Override
	public boolean stop() {
		if (task != null) {
			boolean result = task.cancel(true);
			if (result) {
				executor.shutdown();
				return true;
			}
		}
		return true;
	}

	@Override
	public boolean suspend() {
		return autoTrader.suspend();
	}

	@Override
	public boolean resume() {
		return autoTrader.resume();
	}

	@Override
	public boolean remove() {
		return autoTrader.remove();
	}

	@Override
	public void depositForTrading(BigDecimal amount) {
		autoTrader.depositForTrading(amount);
	}

	@Override
	public void withdrawFromTrading(BigDecimal amount) {
		autoTrader.withdrawFromTrading(amount);
	}

	@Override
	public Optional<BigDecimal> buyMax() {
		return autoTrader.buyMax();
	}

	@Override
	public Optional<BigDecimal> sellMax() {
		return autoTrader.sellMax();
	}

	@Override
	public BigDecimal getCurrentRate(String symbol) {
		return autoTrader.getCurrentRate(symbol);
	}

	@Override
	public String getTickerSymbol() {
		return autoTrader.getTickerSymbol();
	}

	@Override
	public Optional<BigDecimal> buy(BigDecimal quantity) {
		return autoTrader.buy(quantity);
	}

	@Override
	public Optional<BigDecimal> sell(BigDecimal quantity) {
		return autoTrader.sell(quantity);
	}

	@Override
	public boolean isEmailActive() {
		return autoTrader.isEmailActive();
	}

	@Override
	public void setEmailActive(boolean isEmailActive) {
		autoTrader.setEmailActive(isEmailActive);
	}

	@Override
	public MachineState getMachineState() {
		return autoTrader.getMachineState();
	}

	@Override
	public void setMachineState(MachineState machineState) {
		autoTrader.setMachineState(machineState);
	}

	@Override
	public void setPreviousBuy(BigDecimal rate) {
		autoTrader.setPreviousBuy(rate);
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public void setStarted(boolean started) {
		autoTrader.setStarted(started);
	}


	@Override
	public IExchange getExchange() {
		return autoTrader.getExchange();
	}


	@Override
	public IAccount getTradingAccount() {
		return autoTrader.getTradingAccount();
	}


	@Override
	public IStrategy getStrategy() {
		return autoTrader.getStrategy();
	}


	@Override
	public BigDecimal getCurrentRate() {
		return autoTrader.getCurrentRate();
	}


	@Override
	public void setCurrentRate(BigDecimal currentRate) {
		autoTrader.setCurrentRate(currentRate);
	}


	@Override
	public BigDecimal getBuyFee() {
		return autoTrader.getBuyFee();
	}


	@Override
	public void setBuyFee(BigDecimal buyFee) {
		autoTrader.setBuyFee(buyFee);
	}


	@Override
	public BigDecimal getSellFee() {
		return autoTrader.getSellFee();
	}


	@Override
	public void setSellFee(BigDecimal sellFee) {
		autoTrader.setSellFee(sellFee);
		
	}


	@Override
	public Optional<BigDecimal> buy(BigDecimal quantity, BigDecimal rate, IAccount tradingAccount, String tickerSymbol, BigDecimal secondaryBalance) {
		return autoTrader.buy(quantity, rate, tradingAccount, tickerSymbol, secondaryBalance);
	}


	@Override
	public Optional<BigDecimal> sell(BigDecimal quantity, String tickerSymbol, BigDecimal primaryBalance) {
		return autoTrader.sell(quantity, tickerSymbol, primaryBalance);
	}


	@Override
	public StartTime getStartTime() {
		return autoTrader.getStartTime();
	}


	@Override
	public void setStartTime(StartTime startTime) {
		autoTrader.setStartTime(startTime);
	}


	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
	}


}
