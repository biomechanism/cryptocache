/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import java.math.BigDecimal;

public class CTAccount implements IAccount {
	private String name;
	private String primaryCcy;
	private String secondaryCcy;
	private BigDecimal primaryBalance;
	private BigDecimal secondaryBalance;
  
	public CTAccount() {
	}
	
	public CTAccount(IAccount account) {
		this.name = account.getName();
		this.primaryCcy = account.getPrimaryCurrency();
		this.secondaryCcy = account.getSecondaryCurrency();
		this.primaryBalance = account.getPrimaryBalance();
		this.secondaryBalance = account.getSecondaryBalance();
	}
	
	public CTAccount(String name, String primaryCcy, String secondaryCcy, String primaryBalance, String secondaryBalance) {
		this.primaryCcy = primaryCcy;
		this.secondaryCcy = secondaryCcy;
		this.primaryBalance = new BigDecimal(primaryBalance);
		this.secondaryBalance = new BigDecimal(secondaryBalance);
	}
	
	
	@Override
	public BigDecimal getPrimaryBalance() {
		return primaryBalance;
	}
	
	@Override
	public BigDecimal getSecondaryBalance() {
		return secondaryBalance;
	}
	
	
	@Override
	public String getPrimaryCcy() {
		return primaryCcy;
	}
	
	@Override
	public void setPrimaryCcy(String primaryCcy) {
		this.primaryCcy = primaryCcy;
	}

	@Override
	public String getSecondaryCcy() {
		return secondaryCcy;
	}

	@Override
	public void setSecondaryCcy(String secondaryCcy) {
		this.secondaryCcy = secondaryCcy;
	}

	@Override
	public void setPrimaryBalance(String amount) {
		this.primaryBalance = new BigDecimal(amount);
	}
	
	@Override
	public void setSecondaryBalance(String amount)
	{
		this.secondaryBalance = new BigDecimal(amount);
	}
	

	@Override
	public BigDecimal withdrawPrimaryCcy(BigDecimal amount) {
		
		if (amount.compareTo(primaryBalance) <= 0) {
			primaryBalance = primaryBalance.subtract(amount);
			return amount;
		}
		
		amount = primaryBalance;
		primaryBalance = new BigDecimal(0);
		return amount;
	}
	
	@Override
	public void depositPrimaryCcy(BigDecimal amount) {
		primaryBalance = primaryBalance.add(amount);
	}

	@Override
	public BigDecimal withdrawSecondaryCcy(BigDecimal amount) {
		
		if (amount.compareTo(secondaryBalance) <= 0) {
			secondaryBalance = secondaryBalance.subtract(amount);
			return amount;
		}
		
		amount = secondaryBalance;
		secondaryBalance = new BigDecimal(0);
		return amount;
	}

	@Override
	public void depositSecondaryCcy(BigDecimal amount) {
		secondaryBalance = secondaryBalance.add(amount);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String getTickerSymbol() {
		return primaryCcy + secondaryCcy;
	}

	@Override
	public String getPrimaryCurrency() {
		return primaryCcy;
	}

	@Override
	public String getSecondaryCurrency() {
		return secondaryCcy;
	}

	@Override
	public String toString() {
		
		String EOL = System.lineSeparator();
		StringBuilder buffer = new StringBuilder();
		
		buffer.append("Account Name = ").append(name).append(EOL)
				.append("Primary Ccy = ").append(getPrimaryCcy()).append(EOL)
				.append("Primary Balance = ").append(getPrimaryBalance()).append(EOL)
				.append("Secondary Ccy = ").append(getSecondaryCurrency()).append(EOL)
				.append("Secondary Balance = ").append(getSecondaryBalance()).append(EOL);
		
		return buffer.toString();
		
	}
	
}
