/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.exchange;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.OrderSide;
import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.account.NewOrder;
import com.binance.api.client.domain.account.NewOrderResponse;
import com.binance.api.client.domain.general.ExchangeInfo;
import com.binance.api.client.domain.general.FilterType;
import com.binance.api.client.domain.general.SymbolFilter;
import com.binance.api.client.domain.general.SymbolInfo;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.binance.api.client.domain.market.TickerPrice;

import core.CTExchangeResponse;
import core.CandlestickPeriod;
import core.ICTExchangeResponse;
import core.autotrader.MachineState;
import core.exchange.iface.IExchange;

public class BinanceExchange implements IExchange {

	public static final String NAME = "BinanceExchange";
	private static final Logger logger = LoggerFactory.getLogger(BinanceExchange.class);

	private Map<CandlestickPeriod, CandlestickInterval> candleMapping = getCandleMappings();
	
	private BinanceApiRestClient client;
	private ExchangeInfo exchangeInfo;
	private BigDecimal exchangeFee = new BigDecimal("0.00075");
	
	public BinanceExchange(Optional<BinanceApiClientFactory> factory) {
		
		if (factory.isEmpty()) {
			throw new RuntimeException("No factory provided!");
		}

		init(factory.get());

	}
	
	private void init(BinanceApiClientFactory factory) {
		client = factory.newRestClient();
		exchangeInfo = client.getExchangeInfo();
	}
 	
	@Override
	public BigDecimal getRate(String symbol, MachineState buySellState) {
		TickerPrice tickerPrice = client.getPrice(symbol);
		return new BigDecimal(tickerPrice.getPrice());
	}


	@Override
	public BigDecimal getAccountBalance(String ccy) {
		String free = client.getAccount().getAssetBalance(ccy).getFree();
		return new BigDecimal(free);
	}
	
	@Override
	public ICTExchangeResponse buy(String symbol, BigDecimal quantity) {
		
		if (!willPassLotFilter(symbol, quantity)) {
			quantity = modifyAmountForFilter(symbol, quantity);
		}
		
		NewOrderResponse response = client.newOrder(NewOrder.marketBuy(symbol, quantity.toPlainString()));
		if (response.getStatus() == OrderStatus.FILLED || response.getStatus() == OrderStatus.PARTIALLY_FILLED) {
			return createCTResponse(response);
		}
		throw new RuntimeException("Buy Order not fufilled");
	}
	
	@Override
	public ICTExchangeResponse sell(String symbol, BigDecimal quantity) {
		
		if (!willPassLotFilter(symbol, quantity)) {
			quantity = modifyAmountForFilter(symbol, quantity);
		}
		
		
		NewOrderResponse response = client.newOrder(NewOrder.marketSell(symbol, quantity.toPlainString()));
		if (response.getStatus() == OrderStatus.FILLED || response.getStatus() == OrderStatus.PARTIALLY_FILLED) {
			return createCTResponse(response);
		}
		throw new RuntimeException("Sell Order not fufilled");
	}
	
	
	private ICTExchangeResponse createCTResponse(NewOrderResponse binanceResponse) {
		BigDecimal executedQty = new BigDecimal(binanceResponse.getExecutedQty());
		BigDecimal quotedQty = new BigDecimal(binanceResponse.getCummulativeQuoteQty());
		CTExchangeResponse.Status status = mapBinanceToCTStatus(binanceResponse.getStatus());
		CTExchangeResponse.OrderSide orderSide = mapBinanceToCTOrderSide(binanceResponse.getSide());
		return new CTExchangeResponse(executedQty, quotedQty, status, orderSide);
	}
	
	
	private BigDecimal modifyAmountForFilter(String Symbol, BigDecimal amount) {
		SymbolInfo symbolInfo = exchangeInfo.getSymbolInfo(Symbol);
		SymbolFilter priceFilter = symbolInfo.getSymbolFilter(FilterType.LOT_SIZE);
		
		String minQty = priceFilter.getMinQty();
		int scale = getScale(minQty);
		
		return amount.multiply(new BigDecimal("0.998")).setScale(scale, RoundingMode.HALF_DOWN);
		
	}
	
	
	private int getScale(String value) {
		return value.indexOf("1") - 1;
		
	}
	
	//FIXME: Currently only checking minimum.
	private boolean willPassLotFilter(String symbol, BigDecimal amount) {
		SymbolInfo symbolInfo = exchangeInfo.getSymbolInfo(symbol);
		SymbolFilter priceFilter = symbolInfo.getSymbolFilter(FilterType.LOT_SIZE);
		
		boolean filterOK = amount.subtract(new BigDecimal(priceFilter.getMinQty()))
				.remainder(new BigDecimal(priceFilter.getStepSize())).compareTo(BigDecimal.ZERO) == 0;

		return filterOK;
	}
	
	//Basically a direct mapping from Binance.
	private CTExchangeResponse.Status mapBinanceToCTStatus(OrderStatus binanceStatus) {
		return Enum.valueOf(CTExchangeResponse.Status.class, binanceStatus.toString());
	}
	
	private CTExchangeResponse.OrderSide mapBinanceToCTOrderSide(OrderSide binanceStatus) {
		return Enum.valueOf(CTExchangeResponse.OrderSide.class, binanceStatus.toString());
	}



	@Override
	public String getName() {
		return NAME;
	}
	
	
	@Override
	public String toString() {
		return "Exchange Name = " + getName();
	}

	@Override
	public void shutdown() {
		client.shutdown();
	}

	@Override
	public List<Candlestick> getCandlesticks(String symbol, int limit, CandlestickPeriod interval) {
		CandlestickInterval candleInterval = getBinanceCandleInterval(interval);
		return client.getCandlestickBars(symbol, candleInterval, limit, null, null);
	}
	
	
	private Map<CandlestickPeriod, CandlestickInterval> getCandleMappings() {
		Map<CandlestickPeriod, CandlestickInterval> mapping = new HashMap<>();
		mapping.put(CandlestickPeriod.ONE_MINUTE, CandlestickInterval.ONE_MINUTE);
		mapping.put(CandlestickPeriod.THREE_MINUTE, CandlestickInterval.THREE_MINUTES);
		mapping.put(CandlestickPeriod.FIVE_MINUTE, CandlestickInterval.FIVE_MINUTES);
		mapping.put(CandlestickPeriod.FIFTEEN_MINUTE, CandlestickInterval.FIFTEEN_MINUTES);
		mapping.put(CandlestickPeriod.THIRTY_MINUTE, CandlestickInterval.HALF_HOURLY);
		mapping.put(CandlestickPeriod.ONE_HOUR, CandlestickInterval.HOURLY);
		mapping.put(CandlestickPeriod.TWO_HOUR, CandlestickInterval.TWO_HOURLY);
		mapping.put(CandlestickPeriod.FOUR_HOUR, CandlestickInterval.FOUR_HOURLY);
		mapping.put(CandlestickPeriod.SIX_HOUR, CandlestickInterval.SIX_HOURLY);
		mapping.put(CandlestickPeriod.EIGHT_HOUR, CandlestickInterval.EIGHT_HOURLY);
		mapping.put(CandlestickPeriod.TWELVE_HOUR, CandlestickInterval.TWELVE_HOURLY);
		mapping.put(CandlestickPeriod.ONE_DAY, CandlestickInterval.DAILY);
		mapping.put(CandlestickPeriod.ONE_WEEK, CandlestickInterval.WEEKLY);
		mapping.put(CandlestickPeriod.ONE_MONTH, CandlestickInterval.MONTHLY);
		
		return mapping;
	}
	
	
	private CandlestickInterval getBinanceCandleInterval(CandlestickPeriod interval) {
		if (candleMapping.containsKey(interval)) {
			return candleMapping.get(interval);
		}
		
		throw new RuntimeException("Invalid candle interval " + interval);
	}

	@Override
	public BigDecimal getFee() {
		return exchangeFee;
	}


	
	
}
