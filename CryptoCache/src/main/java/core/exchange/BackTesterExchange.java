/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.exchange;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Random;

import com.binance.api.client.domain.market.Candlestick;
import com.google.common.collect.Lists;

import core.CTExchangeResponse;
import core.CTExchangeResponse.OrderSide;
import core.CTExchangeResponse.Status;
import core.CandlestickPeriod;
import core.ICTExchangeResponse;
import core.autotrader.MachineState;
import core.backtester.exception.EndOfBacktestMarketData;
import core.exchange.iface.IExchange;
import core.marketdata.IMarketData;
import core.marketdata.Rate;

public class BackTesterExchange implements IExchange {

	public static final String NAME = "Backtester";
	
	private IMarketData marketData;
	private BigDecimal currentRate;
	private Deque<Rate> rates;
	private Deque<Candlestick> candles;
	private List<Candlestick> progressiveCandles;
	private List<Candlestick> followingCandles;
	
	
	private boolean initialCandleCall = true;
	
	
	//Currently hardcoded to Binance's highest BNB fee.
	private BigDecimal tradeFee = new BigDecimal("0.00075");
	
	public BackTesterExchange() {
	}
	
	public BackTesterExchange(IMarketData marketData) {
		this.marketData = marketData;
		rates = new ArrayDeque<>(marketData.getRates());
	}
	
	
	
	@Override
	public BigDecimal getRate(String symbol, MachineState buySellState) {
		try {
			if (!symbol.equals("BNBTUSD")) {
				this.currentRate = rates.pop().getRate();
				return currentRate;
			} else {
				return getBnbRate();
			}
		} catch (Exception e) {
			throw new EndOfBacktestMarketData("End of Market Data");
		}
	}

	private BigDecimal getBnbRate() {
		double start = 18;
		double end = 28;
		double random = new Random().nextDouble();
		double result = start + (random * (end - start));
		return new BigDecimal(result);
	}

	@Override
	public BigDecimal getAccountBalance(String ccy) {
		throw new org.apache.commons.lang3.NotImplementedException("Not Implemented yet");
	}

	@Override
	public ICTExchangeResponse buy(String symbol, BigDecimal quantity) {
		//double quotedQty = quantity * currentRate;
		BigDecimal quotedQty = quantity.multiply(currentRate);
		return createCTExchangeResponse(quantity, quotedQty, Status.FILLED, OrderSide.BUY);
	}

	@Override
	public ICTExchangeResponse sell(String symbol, BigDecimal quantity) {
		//double quotedQty = quantity * currentRate;
		BigDecimal quotedQty = quantity.multiply(currentRate);
		
		return createCTExchangeResponse(quantity, quotedQty, Status.FILLED, OrderSide.SELL);
	}
	
	
	private ICTExchangeResponse createCTExchangeResponse(BigDecimal buyQty, BigDecimal sellQty, CTExchangeResponse.Status status, CTExchangeResponse.OrderSide orderSide) {
		ICTExchangeResponse response = new CTExchangeResponse(buyQty, sellQty, status , orderSide);
		return response;
	}
	

	public IMarketData getMarketData() {
		return marketData;
	}


	public void setMarketData(IMarketData marketData) {
		this.marketData = marketData;
		
		List<Rate> currentRates = marketData.getRates();
		if (currentRates != null) {
			this.rates = new ArrayDeque<>(marketData.getRates());
		}
		
	}
	
	

	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public String toString() {
		return "Exchange Name = " + getName();
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Candlestick> getCandlesticks(String symbol, int period, CandlestickPeriod interval) {
	
		if (initialCandleCall) {
			initialCandleCall = false;
			List<Candlestick> candles = marketData.getCandles();
			List<Candlestick> initialCandles = candles.subList(0, 40);
			this.progressiveCandles = initialCandles;
			followingCandles = candles.subList(40, candles.size());
			this.candles = initCandleDeque(followingCandles);
			this.currentRate = new BigDecimal(initialCandles.get(initialCandles.size()-1).getClose().trim());
			return initialCandles;
		}
		
		
		if (this.candles.size() > 0) {
			Candlestick candle = this.candles.pop();
			this.currentRate = new BigDecimal(candle.getClose().trim());
			this.progressiveCandles.add(candle);
			return this.progressiveCandles;
		}
		
		return Lists.newArrayList();
		
	}
	
	
	private Deque<Candlestick> initCandleDeque(List<Candlestick> candlesList) {
		Deque<Candlestick> candlesDeque = new ArrayDeque<>();;
		for (Candlestick e : candlesList) {
			candlesDeque.add(e);
		}
		return  candlesDeque;
	}

	@Override
	public BigDecimal getFee() {
		return tradeFee;
	}



}
