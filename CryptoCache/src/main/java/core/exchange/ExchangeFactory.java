/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.exchange;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.api.client.BinanceApiClientFactory;

import core.exchange.iface.IExchange;

public class ExchangeFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(ExchangeFactory.class);
	
	private static String USER_HOME = System.getProperty("user.home");
	private static Map<String, IExchange> exchangeCache = new ConcurrentHashMap<>();
	

	public static IExchange getExchange(String exchange) {
		
		
		IExchange newExchange = switch (exchange) {
		case BinanceExchange.NAME -> new BinanceExchange(getBinanceClientFactory());
		case BinanceWebSocketExchange.NAME -> new BinanceWebSocketExchange(getBinanceClientFactory());
		case BackTesterExchange.NAME -> new BackTesterExchange();
		default -> throw new IllegalArgumentException("Unexpected value: " + exchange);
		};
		
		exchangeCache.put(exchange, newExchange);
		return newExchange;
		
	}
	
	
	private static Optional<BinanceApiClientFactory> getBinanceClientFactory() {
		try (BufferedReader apiReader = new BufferedReader(new FileReader(USER_HOME + "/.cryptotrader/.binance/.apiKey"));
				BufferedReader secretReader = new BufferedReader(new FileReader(USER_HOME + "/.cryptotrader/.binance/.secretKey"))) {

			String apiKey = Objects.requireNonNull(apiReader.readLine());
			String secretKey = Objects.requireNonNull(secretReader.readLine());

			return Optional.of(BinanceApiClientFactory.newInstance(apiKey, secretKey));

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		
		return Optional.empty();

	}
	
}
