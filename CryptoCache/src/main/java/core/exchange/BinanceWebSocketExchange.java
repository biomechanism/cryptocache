/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.exchange;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.channels.IllegalSelectorException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.api.client.BinanceApiCallback;
import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.event.AllMarketTickersEvent;
import com.binance.api.client.domain.market.Candlestick;

import core.CandlestickPeriod;
import core.ICTExchangeResponse;
import core.autotrader.MachineState;
import core.autotrader.iface.INewListingListener;
import core.autotrader.iface.IRateListener;
import core.exchange.iface.IExchange;
import core.exchange.iface.INewListingNotifier;
import core.exchange.iface.IRateNotifier;
import core.exchange.iface.IWebSocketExchange;
import core.marketdata.Rate;

public class BinanceWebSocketExchange implements IExchange, IWebSocketExchange, IRateNotifier, INewListingNotifier {
	
	private static final Logger logger = LoggerFactory.getLogger(BinanceWebSocketExchange.class);
	private static final int RETRY_ATTEMPTS = 5;
	
	public static final String NAME = "BinanceWebSocketExchange";

	private final IExchange exchange;
	private final BinanceApiWebSocketClient wsClient;
	private Closeable wsConnection;

	//TODO: Investigate. Might need to wrap this in an AmtomicReference.
	private Map<String, AllMarketTickersEvent> currentRates = new ConcurrentHashMap<>();
	private Map<String, AllMarketTickersEvent> newListings = new ConcurrentHashMap<>();
	//private Map<String, BigDecimal> sellRates = new ConcurrentHashMap<>();
	
	private final SymbolCallback callback = new SymbolCallback();
	
	private Queue<MarketListener<IRateListener>> rateListeners = new ConcurrentLinkedQueue<>();
	private Queue<INewListingListener> newListingListeners =new ConcurrentLinkedQueue<>();
	
	
	public BinanceWebSocketExchange(Optional<BinanceApiClientFactory> factory) {

		if (factory.isEmpty()) {
			throw new RuntimeException("No factory provided!");
		}

		this.exchange = new BinanceExchange(factory);
		
		this.wsClient = factory.get().newWebSocketClient();
	}
	
			
	@Override
	public void startWebSocketConnection() {
		callback.resetTimeoutRetryAttempts();
		this.wsConnection = this.wsClient.onAllMarketTickersEvent(callback);
	}
	
	@Override
	public void stopWebSocketConnection() {
		try {
			this.wsConnection.close();
			shutdown();
		} catch (IOException e) {
			logger.error("Error closing connection", e);
		}
		
	}
	
			
	@Override
	public String getName() {
		return NAME;
	}

	// TODO: Maybe change return to an Optional.
	@Override
	public BigDecimal getRate(String symbol, MachineState buySellState) {

		if (currentRates.containsKey(symbol)) {
			if (buySellState.isState(MachineState.SELL)) {
				String value = currentRates.get(symbol).getBestBidPrice();
				return new BigDecimal(value);
			} else if (buySellState.isState(MachineState.BUY)) {
				String value = currentRates.get(symbol).getBestAskPrice();
				return new BigDecimal(value);
			}
			return BigDecimal.ZERO;
		}
		
		
		//Fallback, the exchange cache may not have the symbol and rate, as it only receieves 
		//updates on rate changes. Try requesting the rate from the actual exchange.
		BigDecimal rate = this.exchange.getRate(symbol, buySellState);
		if (rate.compareTo(BigDecimal.ZERO) > 0) {
			return rate;
		}

		throw new IllegalSelectorException();
	}

	@Override
	public BigDecimal getAccountBalance(String ccy) {
		return exchange.getAccountBalance(ccy);
	}

	@Override
	public ICTExchangeResponse buy(String symbol, BigDecimal quantity) {
		return exchange.buy(symbol, quantity);
	}

	@Override
	public ICTExchangeResponse sell(String symbol, BigDecimal quantity) {
		return exchange.sell(symbol, quantity);
	}
	
	
	private class SymbolCallback implements BinanceApiCallback<List<AllMarketTickersEvent>> {

		int retryCount = 0;

		@Override
		public void onResponse(List<AllMarketTickersEvent> response) {
			
			if (retryCount > RETRY_ATTEMPTS) {
				retryCount = 0;
			}

			List<Rate> rates = populateNewListings(response);
			populateRates(response);

			publishNewListings(rates);
			publish();
		}

		private void publishNewListings(List<Rate> rates) {

			if (rates.isEmpty()) {
				return;
			}

			for (Rate rate : rates) {
				for (INewListingListener listener : newListingListeners) {
					listener.onNewListing(rate);
				}
			}

		}

		private List<Rate> populateNewListings(List<AllMarketTickersEvent> response) {
			if (currentRates.isEmpty()) {
				// TODO: replace with something that doesn't need to be instattiated every call;
				return new ArrayList<>();
			}

			List<Rate> rateEntries = currentRates.values().stream().filter(e -> e.getSymbol().endsWith("BTC"))
					.map(e -> new Rate(e.getSymbol(), e.getBestAskPrice())).collect(Collectors.toList());

			List<Rate> newRateEntries = response.stream().filter(e -> e.getSymbol().endsWith("BTC"))
					.map(e -> new Rate(e.getSymbol(), e.getBestAskPrice())).collect(Collectors.toList());

			int oldSize = rateEntries.size();
			int newEntries = newRateEntries.size();

			newRateEntries.removeAll(rateEntries);
			return newRateEntries;

		}
		
		
		private void resetTimeoutRetryAttempts() {
			this.retryCount = 0;
		}
		

		private void populateRates(List<AllMarketTickersEvent> response) {
			for (AllMarketTickersEvent entry : response) {
				String symbol = entry.getSymbol();
				currentRates.put(symbol, entry);
			}
		}

		private void publish() {
			for (MarketListener<IRateListener> e : rateListeners) {
				IRateListener listener = e.getListener();
				BigDecimal rate = getRate(e.getSymbol(), listener.getMachineState());
				listener.onRateEvent(rate);
			}
		}

		@Override
		public void onFailure(final Throwable cause) {

			System.err.println("Web socket failed");
			logger.error("WebSockek connection failure", cause);
			retryCount++;
			if (retryCount <= RETRY_ATTEMPTS) {
				long delay = calcretryDelay(retryCount);
				logger.info("Retrying connection in {} second/s...", delay/1000);
				try {
					Thread.sleep(calcretryDelay(retryCount));
					BinanceWebSocketExchange.this.wsConnection = BinanceWebSocketExchange.this.wsClient.onAllMarketTickersEvent(callback);
				} catch (InterruptedException e) {
					logger.error("Interrupted, no further connection retries will be made.");
				}
			} else {
				logger.info("Exhausted retry attempts, no more attempts will be made");
				//TODO: Throw exception to ensure tradre shutdown?s
			}

		}

	}
	
	
	private long calcretryDelay(int retryCount) {
		return retryCount*retryCount*retryCount * 1000;
	}


	@Override
	public BigDecimal getBuyRate(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public BigDecimal getSellRate(String symbol) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void registerRateListener(String symbol, IRateListener listener) {
		
		MarketListener<IRateListener> marketListener = new MarketListener<>(symbol, listener);
		
		if (this.rateListeners.offer(marketListener)) {
			logger.info("Registered listener for {}", symbol);
		}
	}
	
	
	
	//TODO: Allow registration of listening for multiple specified quoted currencies.
	@Override
	public void registerNewListingListener(INewListingListener listener) {
		
		if (newListingListeners.offer(listener)) {
			logger.info("Registered listener for new listings");
		}
		
	}

	
	class MarketListener<T> {
		private final String symbol;
		private final T listener;
		
		public MarketListener(String symbol, T listener) {
			this.symbol = symbol;
			this.listener = listener;
		}

		public String getSymbol() {
			return symbol;
		}

		public T getListener() {
			return listener;
		}
		
	}


	@Override
	public void shutdown() {
		exchange.shutdown();
	}


	@Override
	public List<Candlestick> getCandlesticks(String symbol, int period, CandlestickPeriod interval) {
		return exchange.getCandlesticks(symbol, period, interval);
	}


	@Override
	public BigDecimal getFee() {
		return exchange.getFee();
	}





}
