/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

public class StartTime {

	private static final long defaultPeriod = TimeUnit.DAYS.toSeconds(1);
	
	private int hour;
	private int minute;
	private int second;
	private long period;
	private TimeUnit timeUnit = TimeUnit.SECONDS;

	public StartTime() {
		LocalDateTime date = LocalDateTime.now();
		this.hour = date.getHour();
		this.minute = date.getMinute();
		this.second = date.getSecond();
		this.period = defaultPeriod;
	}	
	
	
	public StartTime(int hour, int minute, int second) {
		this(hour, minute, second, defaultPeriod);
	}
	
	public StartTime(int hour, int minute, int second, long periodInSeconds) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.period = periodInSeconds;
	}
	
	public StartTime(StartTime startTime) {
		this.hour = startTime.getStartHour();
		this.minute = startTime.getStartMinute();
		this.second = startTime.getStartsecond();
		this.period = startTime.getPeriod();
		this.timeUnit = startTime.getTimeUnit();
	}
	
	public int getStartHour() {
		return hour;
	}

	public void setStartHour(int startHour) {
		this.hour = startHour;
	}

	public int getStartMinute() {
		return minute;
	}

	public void setStartMinute(int startMinute) {
		this.minute = startMinute;
	}

	public int getStartsecond() {
		return second;
	}

	public void setStartsecond(int startsecond) {
		this.second = startsecond;
	}
	
	public long getStartDelay() {
		LocalDateTime now = LocalDateTime.now();
		LocalTime specifiedStart = LocalTime.of(hour, minute, second);
		LocalDateTime specifiedDateTime = LocalDateTime.of(LocalDate.now(), specifiedStart);
		
		if (specifiedDateTime.isBefore(now)) {
			specifiedDateTime = specifiedDateTime.plusDays(1);
		}
		
		return Duration.between(now, specifiedDateTime).getSeconds();
		
	}


	public long getPeriod() {
		return period;
	}


	public void setPeriod(long period) {
		this.period = period;
	}


	public TimeUnit getTimeUnit() {
		return timeUnit;
	}


	public void setTimeUnit(TimeUnit timeUnit) {

		this.timeUnit = timeUnit;
	}
	
	
	public LocalTime getLocalTime() { 
		return LocalTime.of(hour, minute, second);
	}
	
	
	
	
}
