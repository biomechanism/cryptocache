/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class CTAttributes {

	private Map<String, String> attributes = new ConcurrentHashMap<>();
	
	public CTAttributes() {
	}
	
	public CTAttributes(CTAttributes ctAttributes) {
		this.attributes.putAll(ctAttributes.getAttributes());
	}
	
	public Optional<String> getAttribute(String name) {
		return Optional.ofNullable(attributes.get(name));
	}
	
	public void setAttribute(String attributeName, String attributeValue) {
		attributes.put(attributeName, attributeValue);
	}
	
	public Map<String, String> getAttributes() {
		return this.attributes;
	}
	
	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		for (Map.Entry<String, String> entry : attributes.entrySet()) {
			buffer.append(entry.getKey()).append(" = ").append(entry.getValue()).append(System.lineSeparator());
		}
		
		return buffer.toString();
	}
	
	
}
