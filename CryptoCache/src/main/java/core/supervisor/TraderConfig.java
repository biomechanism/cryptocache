/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.supervisor;

import core.StartTime;

public class TraderConfig {
	private String cfgName;
	private String primaryCurrency;
	private String secondaryCurrency;
	private String strategy;
	private String exchange;
	private String primaryBalance;
	private String secondaryBalance;
	private String traderType;
	private StartTime startTime;
	

	public TraderConfig(String cfgName, String primaryCcy, String secondaryCcy, String primaryBalance, String secondaryBalance, String strategy, String traderType, String exchange) {
		this.setCfgName(cfgName);
		this.secondaryCurrency = secondaryCcy;
		this.primaryCurrency = primaryCcy;
		this.strategy = strategy;
		this.exchange = exchange;
		this.primaryBalance = primaryBalance;
		this.secondaryBalance = secondaryBalance;
		this.traderType = traderType;
		this.startTime = new StartTime();
	}
	
	public TraderConfig(String cfgName, String primaryCcy, String secondaryCcy, String primaryBalance,
			String secondaryBalance, String strategy, String traderType, String exchange, StartTime startTime) {
		this.setCfgName(cfgName);
		this.secondaryCurrency = secondaryCcy;
		this.primaryCurrency = primaryCcy;
		this.strategy = strategy;
		this.exchange = exchange;
		this.primaryBalance = primaryBalance;
		this.secondaryBalance = secondaryBalance;
		this.traderType = traderType;
		this.startTime = startTime;
	}
	
	public String getSecondaryCurrency() {
		return secondaryCurrency;
	}
	public void setSecondaryCurrency(String accountCurrency) {
		this.secondaryCurrency = accountCurrency;
	}
	public String getPrimaryCurrency() {
		return primaryCurrency;
	}
	public void setPrimaryCurrency(String tradeCurrency) {
		this.primaryCurrency = tradeCurrency;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getCfgName() {
		return cfgName;
	}

	public void setCfgName(String cfgName) {
		this.cfgName = cfgName;
	}

	public String getPrimaryBalance() {
		return primaryBalance;
	}

	public void setPrimaryBalance(String tradeAccountBalance) {
		this.primaryBalance = tradeAccountBalance;
	}
	
	public String getSecondaryBalance() {
		return secondaryBalance;
	}

	public void setSecondaryBalance(String secondaryBalance) {
		this.secondaryBalance = secondaryBalance;
	}

	public StartTime getStartTime() {
		return startTime;
	}

	public void setStartTime(StartTime startTime) {
		this.startTime = startTime;
	}

	public String getTraderType() {
		return traderType;
	}

	public void setTraderType(String traderType) {
		this.traderType = traderType;
	}
	
}
