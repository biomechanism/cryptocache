/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.supervisor;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CTAccount;
import core.CTAttributes;
import core.StartTime;
import core.autotrader.MachineState;
import core.autotrader.NewListingAutoTrader;
import core.autotrader.PollingAutoTrader;
import core.autotrader.RealtimeAutoTrader;
import core.autotrader.iface.IAutoTrader;
import core.emailer.EmailShutdownEvent;
import core.emailer.Emailer;
import core.emailer.iface.IEmailData;
import core.emailer.iface.IEmailShutdownEvent;
import core.exchange.ExchangeFactory;
import core.exchange.iface.IExchange;
import core.marketdata.DefaultMarketDataProvider;
import core.marketdata.IMarketDataProvider;
import core.strategy.AbstractStrategy;
import core.strategy.StrategyFactory;
import core.strategy.iface.IStrategy;

public class Supervisor {

	private static final Logger logger = LoggerFactory.getLogger(Supervisor.class);

	private Properties props = new Properties();

	private final String USER_HOME = System.getProperty("user.home");

	private Optional<Emailer> emailer;
	private final Optional<BlockingDeque<IEmailData>> emailQueue = Optional.of(new LinkedBlockingDeque<>());

	private ExecutorService emailEexecutor;// = Executors.newSingleThreadExecutor();
	private ScheduledExecutorService traderExecutorPool = Executors.newScheduledThreadPool(20);
	private ExecutorService realtimeExecutorPool = Executors.newFixedThreadPool(20);

	private final Map<String, IAutoTrader> autoTraders = new HashMap<>();

	private boolean isEmailActive = false;

	private String emailTo;

	public Supervisor() {
		init();
	}

	private void init() {

		props = new Properties();
		try {

			props.load(new FileReader(USER_HOME + "/.cryptotrader/cryptotrader.properties"));

		} catch (IOException e) {
			logger.info("Unable to create Emailer", e);
		}

		String prop = props.getProperty("email.active");
		isEmailActive = Boolean.valueOf(prop.strip());

		emailTo = props.getProperty("email.to");
		if (emailTo == null || emailTo.isEmpty()) {
			isEmailActive = false;
		}

		emailer = createEmailer();
		emailEexecutor = Executors.newFixedThreadPool(2);
		Runnable runnable = createEmailWorker();
		RunnableFuture<Void> task = new FutureTask<>(runnable, null);
		emailEexecutor.execute(task);

	}

	public IAutoTrader createNewAutoTrader(TraderConfig config) {

		CTAccount tradeAccount = new CTAccount();
		tradeAccount.setPrimaryCcy(config.getPrimaryCurrency());
		tradeAccount.setSecondaryCcy(config.getSecondaryCurrency());
		tradeAccount.setPrimaryBalance(config.getPrimaryBalance());
		tradeAccount.setSecondaryBalance(config.getSecondaryBalance());
		IExchange exchange = getExchange(config.getExchange());
		String cfgName = config.getCfgName();

		IStrategy strategy = StrategyFactory.getStrategy(config.getStrategy());

		strategy.setAttribute(AbstractStrategy.TRADE_FEE, exchange.getFee().toString());

		StartTime startTime = config.getStartTime();

		IMarketDataProvider marketDataProvider = new DefaultMarketDataProvider(exchange);

		// IAutoTrader autoTrader;

		if (config.getTraderType().equals(IAutoTrader.TYPE_POLLING)) {
			return new PollingAutoTrader(cfgName, tradeAccount, exchange, strategy, emailQueue, startTime,
					isEmailActive(), traderExecutorPool, marketDataProvider);
		} else if (config.getTraderType().equals(IAutoTrader.TYPE_REALTIME_NEW_LISTING)) {
			return new NewListingAutoTrader(cfgName, tradeAccount, exchange, strategy, emailQueue, startTime,
					isEmailActive, realtimeExecutorPool, marketDataProvider);
		} else if (config.getTraderType().equals(IAutoTrader.TYPE_REALTIME)) {
			return new RealtimeAutoTrader(cfgName, tradeAccount, exchange, strategy, emailQueue, startTime,
					isEmailActive, realtimeExecutorPool, marketDataProvider);
		}

		throw new IllegalArgumentException("Invalid Trader Type: " + config.getTraderType());
	}

	public boolean registerAutoTrader(IAutoTrader autoTrader) {
		autoTraders.put(autoTrader.getName(), autoTrader);
		return true;
	}

	public IExchange getExchange(String exchange) {
		return ExchangeFactory.getExchange(exchange);
	}

	public TraderConfig createTraderConfig(String cfgName, String primaryCcy, String secondaryCcy,
			String primaryBalance, String secondaryBalance, String strategy, String traderType, String exchange) {
		return new TraderConfig(cfgName, primaryCcy, secondaryCcy, primaryBalance, secondaryBalance, strategy,
				traderType, exchange);

	}

	public TraderConfig createTraderConfig(String cfgName, String primaryCcy, String secondaryCcy,
			String primaryBalance, String secondaryBalance, String strategy, String traderType, String exchange,
			StartTime startTime) {
		return new TraderConfig(cfgName, primaryCcy, secondaryCcy, primaryBalance, secondaryBalance, strategy,
				traderType, exchange, startTime);

	}

	public Optional<IAutoTrader> getAutoTrader(String name) {
		return Optional.ofNullable(autoTraders.get(name));
	}

	public boolean startTrader(String name) {
		Objects.nonNull(name);
		if (autoTraders != null && autoTraders.containsKey(name)) {
			IAutoTrader autoTrader = autoTraders.get(name);
			if (!autoTrader.isStarted() && autoTrader.getMachineState() != MachineState.TERMINATE) {
				logger.info("Starting trader {}", name);
				return autoTrader.start();
			} else {
				// Could not start trader
			}
		}

		logger.info("No such trader ({}), could not start");
		return false;
	}

	public boolean stopTrader(String name) {
		if (autoTraders.containsKey(name)) {
			logger.info("---- Stopping Trader {} ----", name);
			IAutoTrader autoTrader = autoTraders.get(name);
			if (autoTrader.stop()) {
				logger.info("{} STOPPED", name);
				return true;
			}
			logger.error("FAILED TO STOP TRADER: {}", name);
		}
		logger.info("Cannot stop non-existant AutoTrader");
		return false;
	}

	public void startAllTraders() {
		logger.info("---- Starting all traders ----");
		autoTraders.values().forEach(e -> {
			if (!e.isStarted() && e.getMachineState() != MachineState.TERMINATE) {
				if (e.start()) {
					logger.info("Trader: {} started", e.getName());
				} else {
					logger.error("Failed to start trader {}", e.getName());
				}
			}
		});
	}

	public void stopAllTraders() {
		logger.info("Stopping all traders");

		autoTraders.values().forEach(e -> {
			if (e.isStarted()) {
				if (e.stop()) {
					logger.info("Trader {} stopped", e.getName());
				} else {
					logger.error("Failed to stop trader {}", e.getName());
				}
			}
		});

	}

	public void shutdownAllTraders() {
		logger.info("Shutting down all traders");
		autoTraders.values().forEach(e -> e.shutdown());
	}

	public void stopAllAndShutdown() {
		traderExecutorPool.shutdown();
		stopAllTraders();
		shutdownEmailExecutorService();
	}

	public void shutdown() {
		traderExecutorPool.shutdown();
		shutdownEmailExecutorService();
	}

	private boolean shutdownEmailExecutorService() {
		try {
			emailQueue.get().put(new EmailShutdownEvent());
			emailEexecutor.shutdown();
			return true;
		} catch (InterruptedException e) {
			logger.error("Failed to shutdown email worker", e);
			return false;
		}
	}

	public Optional<IAutoTrader> selectTrader(String name) {
		return Optional.ofNullable(autoTraders.get(name));
	}

	public Optional<String> getTickerSymbol(IAutoTrader autoTrader) {
		return Optional.ofNullable(autoTrader.getTickerSymbol());
	}

	public Optional<BigDecimal> getCurrentRate(IAutoTrader autoTrader) {
		BigDecimal currentRate = autoTrader.getCurrentRate(autoTrader.getTickerSymbol());
		return Optional.ofNullable(currentRate);
	}

	public Optional<String> getExchangeNameForSelectedTrader(IAutoTrader autoTrader) {
		return Optional.ofNullable(autoTrader.getName());
	}

	public void setTraderStartTIme(StartTime startTime, IAutoTrader autoTrader) {
		autoTrader.setStartTime(startTime);
	}

	public List<String> listAllTraders() {
		return autoTraders.values().stream().map(IAutoTrader::getName).collect(Collectors.toList());
	}

	public Optional<BigDecimal> buy(BigDecimal amount, IAutoTrader autoTrader) {
		return autoTrader.buy(amount);
	}

	public Optional<BigDecimal> sell(BigDecimal amount, IAutoTrader autoTrader) {
		return autoTrader.sell(amount);
	}

	public Optional<BigDecimal> buyMax(IAutoTrader autoTrader) {
		return autoTrader.buyMax();
	}

	public Optional<BigDecimal> sellMax(IAutoTrader autoTrader) {
		return autoTrader.sellMax();
	}

	public void setTraderModeBuy(IAutoTrader autoTrader) {
		autoTrader.setMachineState(MachineState.BUY);
	}

	public void setTraderModeSell(IAutoTrader autoTrader) {
		autoTrader.setMachineState(MachineState.SELL);
	}

	public void setPreviousBuy(BigDecimal rate, IAutoTrader autoTrader) {
		autoTrader.setPreviousBuy(rate);
	}

	public CTAttributes getAttributes(IAutoTrader autoTrader) {
		return autoTrader.getStrategy().getAttributes();
	}

	public boolean removeTrader(String name) {
		if (autoTraders.containsKey(name)) {
			IAutoTrader autoTrader = autoTraders.get(name);
			if (!autoTrader.isStarted()) {
				autoTrader.shutdown();
				autoTraders.remove(name);
				return true;
			}

			logger.error("Cannot remove an active trader ({})", name);
			return false;

		}
		logger.error("Cannot remove trader {}, trader does not exist", name);
		return false;
	}

	public boolean setAttribute(String name, String value, IAutoTrader autoTrader) {
		if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(value)) {
			IStrategy strategy = autoTrader.getStrategy();
			strategy.setAttribute(name, value);
			return true;
		}
		return false;
	}

	private Optional<Emailer> createEmailer() {
		String user = props.getProperty("email.user");
		Objects.nonNull(user);
		String password = props.getProperty("email.password");
		Objects.nonNull(password);
		String active = props.getProperty("email.active");
		Objects.nonNull(active);
		String to = props.getProperty("email.to");
		Objects.nonNull(active);
		boolean isActive = active != null ? Boolean.valueOf(active) : false;
		return Optional.of(new Emailer(props));
	}

	private Runnable createEmailWorker() {
		return () -> {
			try {
				while (true) {
					
					if (emailQueue.isPresent()) {

						BlockingQueue<IEmailData> queue = emailQueue.get();
						synchronized (queue) {

							IEmailData data = queue.take();

							if (data instanceof IEmailShutdownEvent) {
								return;
							}

							emailer.get().sendEmail(emailTo, data.getSubject(), data.getBody());
						}
					}
				}
				
			} catch (InterruptedException e) {
				logger.error("Failed to queue email", e);
			}
		};
	}

	public boolean isEmailActive() {
		return isEmailActive;
	}

	public void setEmailActive(boolean isEmailActive) {
		this.isEmailActive = isEmailActive;
	}

}
