/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core;

import java.math.BigDecimal;

public class CTExchangeResponse implements ICTExchangeResponse {
	private final BigDecimal executedQuantity;
	private final BigDecimal quotedQuantity;
	private final Status status;
	private final OrderSide orderSide;
	
	public static enum Status {
		NEW, PARTIALLY_FILLED, FILLED, CANCELED, PENDING_CANCEL, REJECTED, EXPIRED;
	};
	
	public static enum OrderSide {
		BUY,
		SELL
	};
	
	
	public CTExchangeResponse(BigDecimal executedQuantity, BigDecimal quotedQuantity, Status status, OrderSide orderSide) {
		this.executedQuantity = executedQuantity;
		this.quotedQuantity = quotedQuantity;
		this.status = status;
		this.orderSide = orderSide;
	}
	
	
	@Override
	public BigDecimal getExecutedQuantity() {
		return executedQuantity;
	}
	@Override
	public BigDecimal getQuotedQuantity() {
		return quotedQuantity;
	}
	@Override
	public Status getStatus() {
		return status;
	}
	@Override
	public OrderSide getOrderSide() {
		return orderSide;
	}
	
	
}
