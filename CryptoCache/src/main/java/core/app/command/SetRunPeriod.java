/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.StartTime;
import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;

public class SetRunPeriod extends AbstractCmd {

	private static final Logger logger = LoggerFactory.getLogger(SetRunPeriod.class);
	
	public SetRunPeriod(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "set run period";
	}

	@Override
	public String alias() {
		return "srp";
	}

	@Override
	public String help() {
		String mesg = "Sets the run interval for polling auto traders";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		setRunPeriod();
		return true;
	}

	private void setRunPeriod() {
		
		Optional<IAutoTrader> selectedTrader = app.getSelectedTrader();
		if (selectedTrader.isPresent()) {
			try {
				System.out.print("\nEnter Run Period (Seconds) > ");
				String period = reader.readLine();
				IAutoTrader autoTrader = selectedTrader.get();
				StartTime startTime = autoTrader.getStartTime();
				startTime.setPeriod(Long.valueOf(period));
				autoTrader.setStartTime(startTime);
				return;
			} catch (IOException e) {
				logger.error("Error setting Run Period");
			}
		}
		app.noTraderSelected();
	}
	
}
