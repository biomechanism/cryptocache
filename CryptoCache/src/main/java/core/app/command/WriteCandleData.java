/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CandlestickPeriod;
import core.app.StartCryptoTraderCLI;
import core.supervisor.Supervisor;

public class WriteCandleData extends AbstractCmd {

	private static final Logger logger = LoggerFactory.getLogger(WriteCandleData.class);
	
	public WriteCandleData(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "write candle data";
	}

	@Override
	public String alias() {
		return "wcd";
	}

	@Override
	public String help() {
		String mesg = "Write candle data to specified file";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		writeCandleData();
		return true;
	}
	
	private void writeCandleData() {

		try {
			System.out.print("\nEnter period> ");
			String period = reader.readLine();
			System.out.print("\nEnter limit> ");
			String limit = reader.readLine();
			System.out.print("\nEnter file name> ");
			String fileName = reader.readLine();

			CandlestickPeriod periodVal = switch (period) {
			case "5m" -> CandlestickPeriod.FIFTEEN_MINUTE;
			case "15m" -> CandlestickPeriod.FIFTEEN_MINUTE;
			case "30m" -> CandlestickPeriod.THIRTY_MINUTE;
			case "4h" -> CandlestickPeriod.FOUR_HOUR;
			case "1d" -> CandlestickPeriod.ONE_DAY;
			default -> CandlestickPeriod.THIRTY_MINUTE;
			};

			int limitVal = Integer.valueOf(limit);
			
			app.getAndWriteCandleData(periodVal, limitVal, fileName);
			
		} catch (IOException e) {
			logger.error("Error reading input");
		}
		
	}

}
