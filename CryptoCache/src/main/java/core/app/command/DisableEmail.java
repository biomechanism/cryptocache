package core.app.command;

import java.io.BufferedReader;
import java.math.BigDecimal;
import java.util.Optional;

import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;

public class DisableEmail extends AbstractCmd {

	public DisableEmail(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "disable email";
	}

	@Override
	public String alias() {
		return "de";
	}

	@Override
	public String help() {
		String mesg = "disables email sending for the elected trader";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		return enableEmail();
	}
	
	private boolean enableEmail() {
		Optional<IAutoTrader> selectedTrader = app.getSelectedTrader();
		
		if (selectedTrader.isPresent()) {
			selectedTrader.get().setEmailActive(false);
			return true;
		}
		app.noTraderSelected();
		return false;
	}

}
