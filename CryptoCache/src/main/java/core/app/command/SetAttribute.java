/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;

public class SetAttribute extends AbstractCmd {

	private static final Logger logger = LoggerFactory.getLogger(SetAttribute.class);

	
	public SetAttribute(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "set attribute";
	}

	@Override
	public String alias() {
		return "sattr";
	}

	@Override
	public String help() {
		String mesg = "Set attribute of currently selected auto trader";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		setAttribute();
		return true;
	}
	
	
	private void setAttribute() {
		Optional<IAutoTrader> selectedTrader = app.getSelectedTrader();
		if (selectedTrader.isPresent()) {
			try {
				System.out.print("\nAttribute Name > ");
				String name = reader.readLine();
				System.out.print("\nAttribute Value > ");
				String value = reader.readLine();
				boolean success = supervisor.setAttribute(name, value, selectedTrader.get());
				if (success) {
					app.ok();
					return;
				}
			} catch (IOException e) {
				logger.error("Failed to set attribute");
			}
		}
		app.noTraderSelected();
		
	}

}
