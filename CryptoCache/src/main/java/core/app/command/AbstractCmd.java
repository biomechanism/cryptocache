/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.util.Map;
import java.util.function.Consumer;

import core.app.StartCryptoTraderCLI;
import core.app.command.iface.ICommandDef;
import core.supervisor.Supervisor;

public abstract class AbstractCmd implements ICommandDef {

	protected Supervisor supervisor;
	protected BufferedReader reader;
	protected StartCryptoTraderCLI app;
	
	public AbstractCmd(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		this.supervisor = supervisor;
		this.reader = reader;
		this.app = app;
	}

}
