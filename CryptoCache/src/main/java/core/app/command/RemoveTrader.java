/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.app.StartCryptoTraderCLI;
import core.supervisor.Supervisor;

public class RemoveTrader extends AbstractCmd {

	private static final Logger logger = LoggerFactory.getLogger(RemoveTrader.class);

	
	public RemoveTrader(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "remove trader";
	}

	@Override
	public String alias() {
		return "rm";
	}

	@Override
	public String help() {
		String mesg = "Specify an existing trader to delete";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		removeTrader();
		return true;
	}

	private void removeTrader() {
		try {
			System.out.print("\nEnter trader to remove> ");
			String traderName = reader.readLine();
			if (supervisor.removeTrader(traderName)) {
				app.ok();
				return;
			}
			app.displayMesg("ERROR: Could not remove trader " + traderName);
			
		} catch (IOException e) {
			logger.error("Failed to remove trader");
		}
	}
	
	
}
