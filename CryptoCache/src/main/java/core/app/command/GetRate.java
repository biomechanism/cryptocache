/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.math.BigDecimal;
import java.util.Optional;

import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;

public class GetRate extends AbstractCmd {

	public GetRate(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "get rate";
	}

	@Override
	public String help() {
		return 
				"""
					get rate - returns the current rate for the selected trader
				""";
	}

	@Override
	public Boolean execute() {
		getRate();
		return true;
	}
	
	
	private BigDecimal getRate() {
		
		Optional<IAutoTrader> selectedTrader = app.getSelectedTrader();
		
		if (selectedTrader.isPresent()) {
			Optional<BigDecimal> rate = supervisor.getCurrentRate(selectedTrader.get());
			long stop = System.currentTimeMillis();

			System.out.printf("Rate: %s\n\n", rate.get());			
			
			return rate.orElse(BigDecimal.ZERO);
		}
		app.noTraderSelected();
		return BigDecimal.ZERO;
	}

	@Override
	public String alias() {
		return "gr";
	}

}
