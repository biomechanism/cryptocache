package core.app.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;

public class BackTest extends AbstractCmd {

	public BackTest(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "backtest";
	}

	@Override
	public String alias() {
		return "bt";
	}

	@Override
	public String help() {
		String mesg = "Backtest the selected trader";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		System.out.println("\n\nNOT YET IMPLEMENTED\n");
		backtest();
		return false;
	}
	
	
	private void backtest() {
		Optional<IAutoTrader> selectedTrader = app.getSelectedTrader();
		if (selectedTrader.isPresent()) {
		}
		
		//app.noTraderSelected();
	}

}
