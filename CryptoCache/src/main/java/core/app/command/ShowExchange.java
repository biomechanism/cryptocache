/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.util.Optional;

import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;

public class ShowExchange extends AbstractCmd {

	public ShowExchange(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "show exchange";
	}

	@Override
	public String alias() {
		return "se";
	}

	@Override
	public String help() {
		String mesg = "Display the exchange of the currently selected auto trader";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		showtraderExchange();
		return true;
	}
	
	
	
	private void showtraderExchange() {
		Optional<IAutoTrader> selectedTrader = app.getSelectedTrader();
		if (selectedTrader.isPresent()) {
			Optional<String> exchangeName = app.getExchangeName(selectedTrader.get());
			if (exchangeName.isPresent()) {
				System.out.println("TRADER EXCHANGE: " + exchangeName);
				return;
			}
		}
		app.noTraderSelected();
	}

}
