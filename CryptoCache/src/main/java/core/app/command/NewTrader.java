/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app.command;

import java.io.BufferedReader;
import java.io.IOException;

import core.app.StartCryptoTraderCLI;
import core.autotrader.iface.IAutoTrader;
import core.supervisor.Supervisor;
import core.supervisor.TraderConfig;

public class NewTrader extends AbstractCmd {

	public NewTrader(Supervisor supervisor, BufferedReader reader, StartCryptoTraderCLI app) {
		super(supervisor, reader, app);
	}

	@Override
	public String command() {
		return "new trader";
	}

	@Override
	public String alias() {
		return "nt";
	}

	@Override
	public String help() {
		String mesg = "Create a new autotrader";
		StringBuilder buffer = new StringBuilder();
		buffer.append("\t(").append(alias()).append("), ").append(command()).append(" - ").append(mesg);
		return buffer.toString();
	}

	@Override
	public Boolean execute() {
		createNewTrader();
		return true;
	}

	
	private void createNewTrader() {
		try {
			System.out.println("--ENTER TRADER DETAILS--");
			System.out.print("\nTrader Name > ");
			String traderName = reader.readLine();
			System.out.print("\nPrimary Ccy > ");
			String primaryCcy = reader.readLine().toUpperCase();
			System.out.print("\nSecondary Ccy > ");
			String secondaryCcy = reader.readLine().toUpperCase();
			System.out.print("\nPrimary Balance > ");
			String primaryBalance = reader.readLine();
			System.out.print("\nSecondary Balance > ");
			String secondaryBalance = reader.readLine();
			System.out.print("\nStrategy Name > ");
			String strategyName = reader.readLine();
			System.out.print("\nTraderType (polling, realtime, newlisting) Name > ");
			String traderType = reader.readLine();
			System.out.print("\nExchange Name > ");
			String exchangeName = reader.readLine();

			TraderConfig config = supervisor.createTraderConfig(traderName, primaryCcy, secondaryCcy, primaryBalance,
					secondaryBalance, strategyName, traderType.toUpperCase(), exchangeName);
			IAutoTrader autoTrader = supervisor.createNewAutoTrader(config);
			supervisor.registerAutoTrader(autoTrader);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
}
