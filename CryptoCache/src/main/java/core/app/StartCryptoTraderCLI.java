/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.api.client.domain.market.Candlestick;
import com.google.common.reflect.ClassPath;

import core.CTAttributes;
import core.CandlestickPeriod;
import core.IAccount;
import core.app.command.iface.ICommandDef;
import core.autotrader.iface.IAutoTrader;
import core.marketdata.MarketDataDImporter;
import core.supervisor.Supervisor;

public class StartCryptoTraderCLI {

	private static final Logger logger = LoggerFactory.getLogger(StartCryptoTraderCLI.class);

	private Map<String, ICommandDef> commandLookup = new LinkedHashMap<>();
	private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	private Supervisor supervisor = new Supervisor();
	private boolean done = false;

	private Optional<IAutoTrader> selectedTrader = Optional.empty();

	public static void main(String[] args) {
		
		logger.info(" ****** STARTING CRYPTO TRADER ******");
		try {
			new StartCryptoTraderCLI().init();
		} catch (ClassNotFoundException e) {
			logger.error("Unable to start. Exiting", e);
		}
	}

	public void quit() {
		done = true;
	}

	public Optional<IAutoTrader> getSelectedTrader() {
		return selectedTrader;
	}
	
	public void setSelectedTrader(IAutoTrader trader) {
		if (trader == null) {
			selectedTrader = Optional.empty();
			return;
		}
		
		selectedTrader = Optional.of(trader);
		
	}

	private void populateCmdMapping(Map<String, ICommandDef> mapping, ICommandDef command) {
		mapping.put(command.command(), command);
		mapping.put(command.alias(), command);
	}

	private void init() throws ClassNotFoundException {

		System.out.println("*** Class loading attempt%n");
		loadCommandClasses();
		readInput();
	}

	private void loadCommandClasses() {

		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		try {
			Class<?> iface = getCommandInterfacec();
			ClassPath cp = ClassPath.from(classLoader);
			for (ClassPath.ClassInfo info : cp.getTopLevelClassesRecursive("core.app.command")) {
				String pkgName = info.getPackageName();
				if ("core.app.command".equals(pkgName)) {
					String fqn = info.getName();

					Class<?> clazz = classLoader.loadClass(fqn);

					if (clazz.isInterface()) {
						continue;
					}

					if (Modifier.isAbstract(clazz.getModifiers())) {
						continue;
					}

					if (!(iface.isAssignableFrom(clazz))) {
						continue;
					}

					Constructor<?> c = clazz.getConstructor(Supervisor.class, BufferedReader.class,
							StartCryptoTraderCLI.class);

					ICommandDef cmd = (ICommandDef) c.newInstance(supervisor, reader, this);

					populateCmdMapping(commandLookup, cmd);

					logger.info("Loaded command: {}", cmd.getClass().getSimpleName());

				}

			}

		} catch (Exception e) {
			logger.error("Error loading commands", e);
		}

	}
	
	

	private Class<?> getCommandInterfacec() throws ClassNotFoundException {
		ClassLoader classLoader = StartCryptoTraderCLI.class.getClassLoader();
		String pkgName = StartCryptoTraderCLI.class.getPackage().getName();
		Class<?> clazz = classLoader.loadClass(pkgName + ".command.iface.ICommandDef");
		return clazz;
	}

	private void readInput() {

		while (!done) {
			try {
				System.out.print("Enter a  CMD: ");
				String cmd = reader.readLine();
				if (cmd == null) {
					continue;
				}
				
				cmd = cmd.toLowerCase().trim();

				if (isDetailHelp(cmd)) {
					System.out.println(retrieveCmd(cmd).help());
					continue;
				}

				if (commandLookup.containsKey(cmd)) {
					commandLookup.get(cmd).execute();
				}

			} catch (IOException e) {
				logger.error("IO Error", e);
			} catch (RuntimeException e) {
				logger.error("Action failed", e);
			}
		}

	}

	private boolean isDetailHelp(String cmd) {
		if (!cmd.equals("help") && cmd.startsWith("help ") || !cmd.equals("h") && cmd.startsWith("h ")) {
			return true;
		}

		return false;
	}

	private ICommandDef retrieveCmd(String cmd) {
		int size = cmd.split(" ")[0].length();
		String param = cmd.substring(size, cmd.length()).trim();
		return commandLookup.get(param);
	}

	public Map<String, ICommandDef> getCommandLookups() {
		return Collections.unmodifiableMap(commandLookup);
	}

	public void shutdown() {
		supervisor.stopAllAndShutdown();
	}

	public boolean selectTrader() {
		try {
			System.out.print("\nEnter trader name > ");
			String traderName = reader.readLine();
			Optional<IAutoTrader> trader = supervisor.getAutoTrader(traderName);
			if (trader.isPresent()) {
				selectedTrader = trader;
				ok();
				return true;
			}

		} catch (IOException e) {
			logger.error("Error selecting trader", e);
		}

		return false;
	}

	private Optional<String> getTickerSymbol() {
		if (selectedTrader.isPresent()) {
			return supervisor.getTickerSymbol(selectedTrader.get());
		}
		noTraderSelected();
		return Optional.empty();
	}

	private String getSelectedTraderName() {
		if (selectedTrader.isPresent()) {
			return selectedTrader.get().getName();
		}
		noTraderSelected();
		return StringUtils.EMPTY;
	}

	public Optional<String> getExchangeName(IAutoTrader autoTrader) {
		return supervisor.getExchangeNameForSelectedTrader(autoTrader);
	}

	public void displayMesg(String mesg) {
		System.out.println(mesg);
	}

	// Should be called from here not the commands. Should be called after a
	// successful command execution.
	public void ok() {
		System.out.println("OK");
	}

	public void noTraderSelected() {
		System.out.println("** No Trader Selected **");
	}

	public void showAttributes() {
		if (selectedTrader.isPresent()) {
			CTAttributes attributes = supervisor.getAttributes(selectedTrader.get());
			System.out.println(attributes);
			return;
		}
		noTraderSelected();
	}

	public void getAndWriteCandleData(CandlestickPeriod periodVal, int limitVal, String fileName) {

		if (selectedTrader.isPresent()) {
			IAutoTrader autoTrader = selectedTrader.get();
			IAccount account = autoTrader.getTradingAccount();
			List<Candlestick> candles = autoTrader.getExchange().getCandlesticks(account.getTickerSymbol(), limitVal, periodVal);

			MarketDataDImporter mdTool = new MarketDataDImporter();
			mdTool.exportClosingPrices(candles, fileName);
		}

	}

}
