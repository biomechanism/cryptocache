/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.app;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CTAccount;
import core.IAccount;
import core.StartTime;
import core.autotrader.PollingAutoTrader;
import core.autotrader.iface.IAutoTrader;
import core.backtester.AutoTraderBackTestWrapper;
import core.backtester.BackTester;
import core.exchange.BackTesterExchange;
import core.marketdata.IMarketData;
import core.marketdata.MarketDataDImporter;
import core.strategy.AbstractMACDStrategy;
import core.strategy.CostAveragingStrategy;
import core.strategy.NewStrategy;
import core.supervisor.Supervisor;
import core.supervisor.TraderConfig;

public class CryptoBackTesterApp {
	
	private static final Logger logger = LoggerFactory.getLogger(CryptoBackTesterApp.class);
	
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader//CryptoTrader/src/main/resources/ETH_USD_2020-07-20_2020-10-19-CoinDesk_SON.csv";
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/ETH_USD_2019-10-08_2020-10-07-CoinDesk_Phil_reversed.csv";
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/ETH_USD_2019-10-08_2020-10-07-CoinDesk_Phil.csv";
	
	private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/YAHOO_ETH-EUR_6M_TO_2020_09_28.csv";
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/candles_btc_busd_30m.csv";
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/candles_link_btc_30m.csv";
	
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/2019-01-01_2020-07-16_eth_clean_asc.csv";
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/ETH-USD_6M_2020_asc.csv";
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/LiteCoin_EUR_6M_2020_asc.csv";
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/TRX_USD_2019-07-21_2020-07-20-CoinDesk_asc.csv";
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/TRX_USD_3M_2020_asc.csv";
	
	//private static final String MDFILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/ETH-USD_6M_2020_07_27_asc.csv";
	
	private static final String CANDLE_MD_FILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/candles_btc_busd_30m.csv";
	
	//private static final String CANDLE_MD_FILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/candles_btc_busd_15m.csv";
	
	//private static final String CANDLE_MD_FILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/candles_btc_busd_2h.csv";
	
	
	//private static final String CANDLE_MD_FILE = "/home/shane/git/cryptotrader/CryptoTrader/src/main/resources/candles_link_btc_30m.csv";
	
	
	private Supervisor supervisor = new Supervisor();
	
	
	public static void main(String[] args) {
		new CryptoBackTesterApp().init2();
	}
	
	
	
	private void init2() {
		
		IMarketData marketData = getMarketData();
		marketData.getRates().remove(0);
		
		String type = IAutoTrader.TYPE_POLLING;
		
//		IAutoTrader autoTrader0 = createTrader("BasicTrader", "ETH", "TUSD", "0", "10000", BasicStrategy.NAME, type,BackTesterExchange.NAME, marketData);
//		IAutoTrader autoTrader1 = createTrader("DefaultTrader", "ETH", "TUSD", "0", "10000", NewStrategyWithBnbBuyBack.NAME, type, BackTesterExchange.NAME, marketData);
//		IAutoTrader autoTrader2 = createTrader("CrashTrader", "ETH", "TUSD", "0", "10000", CrashStrategy.NAME, type, BackTesterExchange.NAME, marketData);
//		IAutoTrader autoTrader3 = createTrader("MaximiseTrader", "ETH", "TUSD", "0", "10000", MaximiseHoldingStrategy.NAME, type, BackTesterExchange.NAME, marketData);
//		IAutoTrader autoTrader4 = createTrader("NewStrategyTrader", "LINK", "BUSD", "0", "1", NewStrategy.NAME, type, BackTesterExchange.NAME, marketData);
		//IAutoTrader autoTrader5 = createTrader("StopLossTrader", "ETH", "TUSD", "1000", "0", TrailingStopLossStrategy.NAME, type, BackTesterExchange.NAME, marketData);
		
		
		IMarketData md = getCandleMarketData();
		IAutoTrader autoTrader5 = createTrader("CostAvgTrader", "BTC", "BUSD", "0", "10000", CostAveragingStrategy.NAME, type,BackTesterExchange.NAME, md);
		
		
		
		supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader5));
//		supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader1));
//		supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader2));
		//supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader3));
		
///		IAutoTrader autoTrader3 = createTrader("MaximiseTrader", "BTC", "BUSD", "0", "10000", MaximiseHoldingStrategy.NAME, type, BackTesterExchange.NAME, marketData);
///		IAutoTrader autoTrader4 = createTrader("NewStrategyTrader", "BTC", "BUSD", "0", "10000", NewStrategy.NAME, type, BackTesterExchange.NAME, marketData);
		//supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader3));
		//supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader4));
		
	//	supervisor.registerAutoTrader(new AutoTraderBackTestWrapper(autoTrader5));
		
		
		//initMACDTrader();
		
		
		supervisor.startAllTraders();
		//supervisor.shutdown();
		
	}

	
	private void initMACDTrader() {
		String type = IAutoTrader.TYPE_POLLING;
		IMarketData marketData = getCandleMarketData();
		IAutoTrader autoTrader = createTrader("MACDTrader", "BTC", "BUSD", "0", "10000", NewStrategy.NAME, type, BackTesterExchange.NAME, marketData);
		autoTrader.getStrategy().setAttribute(AbstractMACDStrategy.PROFIT_PERCENT_TRIGGER, "0.01");
		supervisor.registerAutoTrader(autoTrader);
		
	}
	
	
	public BackTester createBackTester(String name, IAccount tradingAccount, IAutoTrader autoTrader) {
		BackTester backtester = new BackTester(name, tradingAccount, autoTrader);
		return backtester;
	}
	
	
	
	public void setBackTesterExchangeMarketData(IAutoTrader autoTrader, IMarketData marketData) {
		((BackTesterExchange)((PollingAutoTrader)autoTrader).getExchange()).setMarketData(marketData);
	}
	
	
	public IAutoTrader createTrader(String name, String primaryCcy, String secondaryCcy, String primaryBalance,
			String secondaryBalance, String strategyName, String traderType, String exchangeName, IMarketData marketData) {
		
		LocalDateTime dateTime = LocalDateTime.now();
		StartTime startTime = new StartTime();
		startTime.setPeriod(1L);
		startTime.setTimeUnit(TimeUnit.NANOSECONDS);
		startTime.setStartHour(dateTime.getHour());
		TraderConfig config = supervisor.createTraderConfig(name, primaryCcy, secondaryCcy, primaryBalance, secondaryBalance, strategyName, traderType, exchangeName, startTime);
		IAutoTrader autoTrader = supervisor.createNewAutoTrader(config);
		setBackTesterExchangeMarketData(autoTrader, marketData);
		
		return autoTrader;
	}
	
	
	protected IMarketData getMarketData() {
		IMarketData marketData = new MarketDataDImporter().importData(MDFILE);
		return marketData;
	}
	
	
	protected IMarketData getCandleMarketData() {
		IMarketData marketData = new MarketDataDImporter().importCandleData(CANDLE_MD_FILE);
		return marketData;
	}
	
	
	protected IAccount createAccount(String name, String primaryBalance, String primaryCcy, String secondaryBalance, String secondaryCcy) {
		IAccount account = new CTAccount(name, primaryCcy, secondaryCcy, primaryBalance, secondaryBalance);
		return account;
	}

}
