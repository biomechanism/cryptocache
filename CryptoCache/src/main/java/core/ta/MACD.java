/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.ta;

import java.util.Collections;
import java.util.List;

public class MACD {
	
	private List<Double> shortEMA = null;
	private List<Double> longEMA = null;
	private List<Double> macdData = null;
	
	
	public MACD() {
	}
	
	public MACD(List<Double> prices, int longPeriod, int shortPeriod) {
		EMA longEMA = new EMA(prices, longPeriod);
		EMA shortEMA = new EMA(prices, shortPeriod);
		this.macdData = shortEMA.subtractEMA(longEMA);
	}
 
	
	public Double getMACD(List<Double> prices, int longPeriod, int shortPeriod) {
		EMA longEMA = new EMA(prices, longPeriod);
		EMA shortEMA = new EMA(prices, shortPeriod);
		this.macdData = shortEMA.subtractEMA(longEMA);
		return this.macdData.get(macdData.size()-1);
	}
	
	
	public List<Double> getMACDData() {
		
		if (this.macdData == null) {
			throw new RuntimeException("MACD data has not been calculated");
		}
		
		return Collections.unmodifiableList(this.macdData);
	}
	
	
	public List<Double> getShortEMA() {
		
		if (this.shortEMA == null) {
			throw new RuntimeException("EMA data has not been calculated");
		}
		
		return Collections.unmodifiableList(this.shortEMA);
	}
	
	
	public List<Double> getLongtEMA() {
		
		if (this.longEMA == null) {
			throw new RuntimeException("EMA data has not been calculated");
		}
		
		return Collections.unmodifiableList(this.longEMA);
	}
	
	
	
}
