/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.ta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.util.Precision;

public class EMA {

	private final int period;
	private double weight;
	private List<Double> prices;
	private List<Double> emaData;
	
	public EMA(int period) {
		this.period = period;
	}
	
	public EMA(List<Double> prices, int period) {
		this.weight = (2.0D / (period + 1.0D));
		this.prices = prices;
		this.period = period;
		calculate(period);
	}
	
	
	public EMA(List<Double> prices, int period, SMA sma) {
		this.weight = (2.0D / (period + 1.0D));
		this.prices = prices;
		this.period = period;
		calculate(period, sma);
	}
	
	
	protected void calculate(int period) {
		this.emaData = calcEMA(this.prices, period);
	}
	
	
	protected void calculate(int period, SMA sma) {
		this.emaData = calcEMAViaSMA(this.prices, period, sma);
	}
	
	
	private List<Double> calcEMA(List<Double> data, int period) {
		return doCalc(data, data.get(0));
	}
	
	
	private List<Double> calcEMAViaSMA(List<Double> data, int period, SMA sma) {
		double smaVal = sma.getSMA();
		List<Double> subData = data.subList(sma.getPeriod(), data.size());
		return doCalc(subData, smaVal);
	}
	
	
	private List<Double> doCalc(List<Double> prices, double startSeed) {
		List<Double> emas = new ArrayList<>();
		double previousEma;
		
		for (int i = 0 ; i < prices.size(); i++) {
			if (i == 0) {
				previousEma = startSeed;
			} else {
				previousEma = emas.get(i - 1);
			}
			
			double price = prices.get(i);
			double ema = ((price * weight) + (previousEma * (1 - weight)));
			double rounded = Precision.round(ema, 8);
			emas.add(rounded);
		}
		return emas;
	}
	
	public Double getEMA(List<Double> prices, int period) {
		this.prices = prices;
		this.emaData = calcEMA(prices, period);
		return emaData.get(emaData.size()-1);
	}
	
	
	public double getPeriod() {
		return this.period;
	}
	
	public List<Double> getEMAData() {
		return Collections.unmodifiableList(this.emaData);
	}
	
	
	public Double getLatestEMA() {
		return emaData.get(emaData.size()-1);
	}
	
	
	public List<Double> subtractEMA(EMA ema) {
		
		List<Double> subEmaData = ema.getEMAData();
		
		if (this.emaData.size() != subEmaData.size()) {
			throw new RuntimeException("EMA data must be the same length");
		}
		
		List<Double> values = new ArrayList<>();

		for (int i = 0; i < this.emaData.size(); i++) {
			Double result = this.emaData.get(i) - subEmaData.get(i);
			values.add(result);
		}
		
		return values;
	}
	
	
}
