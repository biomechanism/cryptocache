/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.autotrader;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.IAccount;
import core.StartTime;
import core.emailer.iface.IEmailData;
import core.exchange.iface.IExchange;
import core.marketdata.IMarketDataProvider;
import core.marketdata.Rate;
import core.strategy.iface.IStrategy;

public class NewListingAutoTrader extends RealtimeAutoTrader {
	private static final Logger logger = LoggerFactory.getLogger(NewListingAutoTrader.class);

	public NewListingAutoTrader(String name, IAccount tradeingAccount, IExchange exchange, IStrategy strategy,
			Optional<BlockingDeque<IEmailData>> emailQueue, StartTime startTime, boolean isEmailActive, ExecutorService executorService,
			IMarketDataProvider marketDataProvider) {
		super(name, tradeingAccount, exchange, strategy, emailQueue, startTime, isEmailActive, executorService, marketDataProvider);
	}
	
	@Override
	public void onRateEvent(BigDecimal rate) {
	}


	@Override
	public void onNewListing(Rate rate) {
		logger.info("========= NEW LISTING : {}, Rate: {}", rate.getSymbol(), rate.getRate());
	}

}
