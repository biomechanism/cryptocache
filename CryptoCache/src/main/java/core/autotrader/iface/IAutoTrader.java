/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.autotrader.iface;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import core.IAccount;
import core.StartTime;
import core.autotrader.MachineState;
import core.exchange.iface.IExchange;
import core.strategy.iface.IStrategy;

public interface IAutoTrader extends Runnable {
	
	String TYPE_REALTIME = "REALTIME";
	String TYPE_POLLING = "POLLING";
	String TYPE_REALTIME_NEW_LISTING = "NEWLISTING";
	
	String getName();
	
	boolean start();

	boolean stop();

	boolean suspend();

	boolean resume();

	boolean remove();

	void depositForTrading(BigDecimal amount);

	void withdrawFromTrading(BigDecimal amount);
	
	Optional<BigDecimal> buyMax();
	
	Optional<BigDecimal> sellMax();

	//boolean setStartDelay(long seconds);
	
	boolean start(long delay, long period, TimeUnit timeUnit);

	BigDecimal getCurrentRate(String symbol);

	String getTickerSymbol();

	Optional<BigDecimal> buy(BigDecimal quantity);

	Optional<BigDecimal> sell(BigDecimal quantity);

	boolean isEmailActive();

	void setEmailActive(boolean isEmailActive);

	MachineState getMachineState();

	void setMachineState(MachineState machineState);

	void setPreviousBuy(BigDecimal rate);

	boolean isStarted();

	void setStarted(boolean started);

	IExchange getExchange();

	IAccount getTradingAccount();

	IStrategy getStrategy();

	BigDecimal getCurrentRate();

	void setCurrentRate(BigDecimal currentRate);

	BigDecimal getBuyFee();

	void setBuyFee(BigDecimal buyFee);

	BigDecimal getSellFee();

	void setSellFee(BigDecimal sellFee);

	Optional<BigDecimal> buy(BigDecimal quantity, BigDecimal rate, IAccount tradingAccount, String tickerSymbol, BigDecimal secondaryBalance);

	Optional<BigDecimal> sell(BigDecimal quantity, String tickerSymbol, BigDecimal primaryBalance);

	StartTime getStartTime();
	
	void setStartTime(StartTime startTime);

	void shutdown();


}
