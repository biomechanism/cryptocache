/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.autotrader;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import core.CTExchangeResponse.Status;
import core.IAccount;
import core.ICTExchangeResponse;
import core.StartTime;
import core.autotrader.iface.IAutoTrader;
import core.autotrader.iface.INewListingListener;
import core.autotrader.iface.IRateListener;
import core.emailer.EmailData;
import core.emailer.iface.IEmailData;
import core.exchange.iface.IExchange;
import core.exchange.iface.INewListingNotifier;
import core.exchange.iface.IRateNotifier;
import core.exchange.iface.IWebSocketExchange;
import core.marketdata.IMarketDataProvider;
import core.strategy.iface.IStrategy;

public abstract class AbstractAutoTrader implements IAutoTrader {

	private static final Logger logger = LoggerFactory.getLogger(AbstractAutoTrader.class);
	protected final String name;
	protected boolean started = false;
	protected IAccount tradingAccount;
	protected IExchange exchange;
	protected IStrategy strategy;
	protected volatile ExecutorService executor;
	protected final Optional<BlockingDeque<IEmailData>> emailQueue;
	protected volatile Future<?> task;
	protected boolean isEmailActive;
	protected StartTime startTime;
	protected BigDecimal currentRate = BigDecimal.ZERO;
	protected BigDecimal buyFeeAmount = BigDecimal.ZERO;
	protected BigDecimal sellFeeAmount = BigDecimal.ZERO;
	protected MachineState machineState = MachineState.BUY;
	protected IMarketDataProvider marketDataProvider;
	
	public AbstractAutoTrader(String name, IAccount tradeingAccount, IExchange exchange, IStrategy strategy,
			Optional<BlockingDeque<IEmailData>> emailQueue, StartTime startTime, boolean isEmailActive,
			ExecutorService executorService, IMarketDataProvider marketDataProvider) {

		this.name = name;
		this.tradingAccount = tradeingAccount;
		this.exchange = exchange;
		this.strategy = strategy;
		this.emailQueue = emailQueue;
		this.executor = executorService;
		this.startTime = startTime;
		this.isEmailActive = isEmailActive;
		this.marketDataProvider = marketDataProvider;
	
	}
	

	@Override
	public String getName() {
		return name;
	}
	

	@Override
	public MachineState getMachineState() {
		return machineState;
	}


	@Override
	public boolean start() {
		long startDelay = startTime.getStartDelay();
		long periodDelay = startTime.getPeriod();
		TimeUnit timeUnit = startTime.getTimeUnit();
		return start(startDelay, periodDelay, timeUnit);
	}
	
	
	@Override
	public boolean start(long delay, long period, TimeUnit timeUnit) {
		
		if (this.exchange instanceof IWebSocketExchange) {
			IWebSocketExchange wsExchange = ((IWebSocketExchange)exchange);
			if (wsExchange instanceof IRateNotifier && this instanceof IRateListener) {
				logger.info("Registering rate listener with exchange");
				registerRateListener(((IRateListener)this), ((IRateNotifier)wsExchange));
			} 
		
			if (wsExchange instanceof INewListingNotifier && this instanceof INewListingListener) {
				registerNewListingListener(((INewListingListener)this), ((INewListingNotifier)wsExchange));
			}
			
			wsExchange.startWebSocketConnection();
		}
		
		if (!started) {
			task = scheduleTask(delay, period, timeUnit);
			started = true;
			return true;
		}
		logger.error("Trader {} is already active", getName());
		return false;
	}
	
	
	private void registerNewListingListener(INewListingListener listener, INewListingNotifier notifier) {
		// TODO Auto-generated method stub
		
	}


	private void registerRateListener(IRateListener listener, IRateNotifier notifier) {
		notifier.registerRateListener(this.tradingAccount.getTickerSymbol(), listener);
	}

	
	private Future<?> scheduleTask(long delay, long period, TimeUnit timeunit) {
		if (executor instanceof ScheduledExecutorService) {
			return ((ScheduledExecutorService)executor).scheduleAtFixedRate(this, delay, period, timeunit);
		} else if (executor instanceof ExecutorService) {
			return executor.submit(this);
		}
		throw new IllegalArgumentException("ExecutorService type is not supported - " + executor.getClass().getSimpleName());
	}


	@Override
	public boolean stop() {

		if (task != null) {
			boolean result = task.cancel(true);
			if (result) {
				started = false;
				return true;
			}
		}
		
		setMachineState(MachineState.TERMINATE);
		
		return true;
	}
	
	
	@Override
	public void shutdown() {
		if (this.exchange instanceof IWebSocketExchange) {
			((IWebSocketExchange)exchange).stopWebSocketConnection();
		} else {
			this.exchange.shutdown();
		}
		
		stop();
	}
	
	
	@Override
	public Optional<BigDecimal> buy(BigDecimal quantity) {
		String tickerSymbol = tradingAccount.getTickerSymbol();
		BigDecimal secondaryBalance = tradingAccount.getSecondaryBalance();
		return buy(quantity, currentRate, tradingAccount, tickerSymbol, secondaryBalance);
	}

	
	@Override
	public Optional<BigDecimal> buy(BigDecimal quantity, BigDecimal rate, IAccount tradingAccount, String tickerSymbol,
			BigDecimal secondaryBalance) {
		logger.info("{} BUY {}", tickerSymbol, quantity);
		try {
			if (secondaryBalance.compareTo(quantity.multiply(rate)) >= 0) {
				ICTExchangeResponse response = exchange.buy(tickerSymbol, quantity);
				tradingAccount.withdrawSecondaryCcy(response.getQuotedQuantity());
				tradingAccount.depositPrimaryCcy(response.getExecutedQuantity());
				// setBuyFee(calcBuyFee(response));
				
				if (isEmailActive()) {
					String subject = String.format("[CryptoTrader] - [%s - %s] CT BUY %s @ %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), currentRate);

					String mesg = String.format("[%s - %s] BUY %s @ %s = %s %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), currentRate, response.getExecutedQuantity(), tradingAccount.getPrimaryCcy());

					queueEmail(subject, mesg);
				}
				
				return Optional.of(response.getExecutedQuantity());
			}

			logger.error("Failed to buy");
			return Optional.empty();
		} catch (RuntimeException e) {
			logger.error("Sever failure trying to buy.", e);
			return Optional.empty();
		}
	}
	

	@Override
	public Optional<BigDecimal> sell(BigDecimal quantity) {
		BigDecimal primaryBalance = tradingAccount.getPrimaryBalance();
		String tickerSymbol = tradingAccount.getTickerSymbol();
		return sell(quantity, tickerSymbol, primaryBalance);
	}

	
	@Override
	public Optional<BigDecimal> sell(BigDecimal quantity, String tickerSymbol, BigDecimal primaryBalance) {
		logger.info("{} SELL {}", tickerSymbol, quantity);
		try {

			// primaryBalance >= quantity
			if (primaryBalance.compareTo(quantity) >= 0) {
				setSellFee(quantity);
				ICTExchangeResponse response = exchange.sell(tickerSymbol, quantity);
				tradingAccount.withdrawPrimaryCcy(response.getExecutedQuantity());
				tradingAccount.depositSecondaryCcy(response.getQuotedQuantity());
				
				if (isEmailActive()) {
					String subject = String.format("[CryptoTrader] - [%s - %s] CT SELL %s @ %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), currentRate);

					String mesg = String.format("[%s - %s] SELL %s @ %s = %s %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), currentRate, response.getQuotedQuantity(), tradingAccount.getSecondaryCurrency());

					queueEmail(subject, mesg);
				}
				
				
				return Optional.of(response.getQuotedQuantity());
			}

			logger.error("Failed to sell");
			return Optional.empty();
		} catch (RuntimeException e) {
			logger.error("Sever failure trying to sell.", e);
			return Optional.empty();
		}
	}
	
	
	@Override
	public Optional<BigDecimal> buyMax() {

		try {
			String tickerSymbol = tradingAccount.getTickerSymbol();
			String symbol = tradingAccount.getTickerSymbol();
			BigDecimal secondaryBalance = tradingAccount.getSecondaryBalance();

			// double quantity = secondaryBalance / rate;
			BigDecimal quantity = secondaryBalance.divide(currentRate, 8, RoundingMode.HALF_DOWN);

			ICTExchangeResponse response = exchange.buy(symbol, quantity);
			if (response.getStatus() == Status.FILLED || response.getStatus() == Status.PARTIALLY_FILLED) {
				setBuyFee(calcBuyFee(response));
				tradingAccount.withdrawSecondaryCcy(response.getQuotedQuantity());
				tradingAccount.depositPrimaryCcy(response.getExecutedQuantity());

				if (isEmailActive()) {
					String subject = String.format("[CryptoTrader] - [%s - %s] CT BUY %s @ %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), currentRate);

					String mesg = String.format("[%s - %s] BUY %s @ %s = %s %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), currentRate, response.getExecutedQuantity(), tradingAccount.getPrimaryCcy());

					queueEmail(subject, mesg);
				}

				logger.info("[{}] BUY {} @ {} = {} {}", strategy.getName(), tradingAccount.getPrimaryCurrency(), currentRate,
						response.getExecutedQuantity(), tradingAccount.getPrimaryCcy());

				return Optional.of(response.getExecutedQuantity());
			}
		} catch (RuntimeException e) {
			logger.error("Failed during buyMAx() call", e);
			stop();
		}

		return Optional.empty();

	}

	@Override
	public Optional<BigDecimal> sellMax() {
		BigDecimal rate = currentRate;
		BigDecimal amount = tradingAccount.getPrimaryBalance();
		String symbol = tradingAccount.getTickerSymbol();

		try {
			BigDecimal sellFee = calcSellFee(amount, rate);

			ICTExchangeResponse response = exchange.sell(symbol, amount);
			if (response.getStatus() == Status.FILLED || response.getStatus() == Status.PARTIALLY_FILLED) {
				tradingAccount.withdrawPrimaryCcy(response.getExecutedQuantity());
				tradingAccount.depositSecondaryCcy(response.getQuotedQuantity());
				setSellFee(sellFee);

				if (isEmailActive()) {
					String subject = String.format("[CryptoTrader] - [%s - %s] CT SELL %s @ %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), rate);

					String mesg = String.format("[%s - %s] SELL %s @ %s = %s %s", getName(), strategy.getName(),
							tradingAccount.getPrimaryCurrency(), rate, response.getQuotedQuantity(), tradingAccount.getSecondaryCurrency());

					queueEmail(subject, mesg);
				}

				logger.info("[{}] SELL {} @ {} = {} {}", strategy.getName(), tradingAccount.getPrimaryCurrency(), rate,
						response.getQuotedQuantity(), tradingAccount.getSecondaryCurrency());

				return Optional.of(response.getQuotedQuantity());
			}
		} catch (RuntimeException e) {
			logger.error("Failed during sellMAx() call", e);
			stop();
		}

		return Optional.empty();
	}
	
	
	private BigDecimal calcBuyFee(ICTExchangeResponse response) {
		BigDecimal amount = response.getQuotedQuantity();
		BigDecimal buyFeeAmount = amount.multiply(strategy.getFee());
		return buyFeeAmount;
	}

	
	// Needs to be called before the sell decision to include the BNB buy-back fee,
	// so we cannot use the response object here.
	private BigDecimal calcSellFee(BigDecimal primaryAmount, BigDecimal rate) {
		BigDecimal sellFeeAmount = primaryAmount.multiply(strategy.getFee()).multiply(rate);
		return sellFeeAmount;
	}

	
	private void queueEmail(String subject, String body) {
		if (emailQueue.isPresent()) {
			try {
				emailQueue.get().put(new EmailData(subject, body));
			} catch (InterruptedException e) {
				logger.error("Failed to queue email", e);
			}
		}
	}
	
	
	@Override
	public boolean suspend() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean resume() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void depositForTrading(BigDecimal amount) {
		// TODO Auto-generated method stub

	}

	@Override
	public void withdrawFromTrading(BigDecimal amount) {
		// TODO Auto-generated method stub

	}


	@Override
	public BigDecimal getCurrentRate(String symbol) {
		return exchange.getRate(symbol, machineState);
	}

	@Override
	public String getTickerSymbol() {
		return tradingAccount.getTickerSymbol();
	}


	@Override
	public boolean isEmailActive() {
		return isEmailActive;
	}

	@Override
	public void setEmailActive(boolean isEmailActive) {
		this.isEmailActive = isEmailActive;
	}

	@Override
	public void setMachineState(MachineState machineState) {
		this.machineState = machineState;
	}

	@Override
	public void setPreviousBuy(BigDecimal rate) {
		strategy.setPreviousBuy(rate);
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public void setStarted(boolean started) {
		this.started = started;
	}

	@Override
	public IExchange getExchange() {
		return exchange;
	}

	@Override
	public IAccount getTradingAccount() {
		return tradingAccount;
	}

	@Override
	public IStrategy getStrategy() {
		return strategy;
	}

	@Override
	public BigDecimal getCurrentRate() {
		return currentRate;
	}

	@Override
	public void setCurrentRate(BigDecimal currentRate) {
		this.currentRate = currentRate;
	}

	@Override
	public BigDecimal getBuyFee() {
		return buyFeeAmount;
	}

	@Override
	public void setBuyFee(BigDecimal buyFee) {
		this.buyFeeAmount = buyFee;
	}

	@Override
	public BigDecimal getSellFee() {
		return sellFeeAmount;
	}

	@Override
	public void setSellFee(BigDecimal sellFee) {
		this.sellFeeAmount = sellFee;
	}

	@Override
	public StartTime getStartTime() {
		return new StartTime(this.startTime);
	}

	@Override
	public void setStartTime(StartTime startTime) {
		this.startTime = startTime;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAutoTrader other = (AbstractAutoTrader) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() {
		String EOL = System.lineSeparator();
		StringBuilder buffer = new StringBuilder();
		buffer.append("Trader Name = ").append(getName()).append(EOL)
				.append("Started = ").append(started).append(EOL)
				.append("State = ").append(machineState).append(EOL)
				.append("Start Time = ").append(startTime.getLocalTime()).append(EOL)
				.append("Run Period = ").append(startTime.getPeriod()).append(EOL);

		return buffer.toString();

	}

}
