/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.marketdata;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.binance.api.client.domain.market.Candlestick;

import core.CandlestickPeriod;
import core.autotrader.MachineState;
import core.exchange.iface.IExchange;
import core.ta.EMA;
import core.ta.MACD;
import core.ta.SMA;

public class DefaultMarketDataProvider implements IMarketDataProvider {

	private IExchange exchange;
	
	public DefaultMarketDataProvider(IExchange exchange) {
		this.exchange = exchange;
	}
	
	@Override
	public List<Candlestick> getCandlesticks(String symbol, int limit, CandlestickPeriod interval) {
		return exchange.getCandlesticks(symbol, limit, interval);
	}

	@Override
	public BigDecimal getEMA(List<Candlestick> candles, int emaPeriod) {
		List<Double> closingPrices = candles.stream().map(e -> Double.valueOf(e.getClose())).collect(Collectors.toList());
		EMA ema = new EMA(closingPrices, emaPeriod);
		return BigDecimal.valueOf(ema.getLatestEMA());
	}

	@Override
	public BigDecimal getSMA(List<Candlestick> candles) {
		List<Double> closingPrices = candles.stream().map(e -> Double.valueOf(e.getClose())).collect(Collectors.toList());
		SMA sma = new SMA(closingPrices, candles.size()-1);
		return BigDecimal.valueOf(sma.getSMA());
	}

	@Override
	public BigDecimal getMACD(List<Candlestick> candles, int shortEMA, int longEMA) {
		List<Double> closingPrices = candles.stream().map(e -> Double.valueOf(e.getClose())).collect(Collectors.toList());
		MACD macd = new MACD();
		return BigDecimal.valueOf(macd.getMACD(closingPrices, longEMA, shortEMA));
	}

	@Override
	public BigDecimal getRate(String symbol, MachineState buySellState) {
		return exchange.getRate(symbol, buySellState);
	}

	@Override
	public BigDecimal getSignal(List<Candlestick> candles, int shortEMA, int longEMA, int signalEMA) {
		List<Double> closingPrices = candles.stream().map(e -> Double.valueOf(e.getClose())).collect(Collectors.toList());
		MACD macd = new MACD(closingPrices, longEMA, shortEMA);
		EMA ema = new EMA(macd.getMACDData(), signalEMA);
		return BigDecimal.valueOf(ema.getLatestEMA());
	}

}
