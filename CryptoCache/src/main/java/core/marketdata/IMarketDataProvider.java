/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.marketdata;

import java.math.BigDecimal;
import java.util.List;

import com.binance.api.client.domain.market.Candlestick;

import core.CandlestickPeriod;
import core.autotrader.MachineState;

public interface IMarketDataProvider {

	List<Candlestick> getCandlesticks(String symbol, int limit, CandlestickPeriod interval);
	BigDecimal getEMA(List<Candlestick> candles, int emaPeriod);
	BigDecimal getSMA(List<Candlestick> candles);
	BigDecimal getMACD(List<Candlestick> candles, int shortEMA, int longEMA);
	BigDecimal getRate(String symbol, MachineState buySellState);
	BigDecimal getSignal(List<Candlestick> candles, int shortEMA, int longEMA, int signalEMA);
	
	
}
