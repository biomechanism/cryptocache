/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.marketdata;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Rate {
	private LocalDate date;
	private String symbol;
	private String rate;
	
	
	public Rate(String symbol, String rate) {
		this.symbol = symbol;
		this.rate = rate;
	}
	
	public Rate(String rate) {
		this.rate = rate;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
	public void setSymbol(String currency) {
		this.symbol = currency;
	}
	
	public BigDecimal getRate() {
		return new BigDecimal(rate);
	}
	
	public void setRate(String rate) {
		this.rate = rate;
	}

	
	
	//TODO: Possibly change this to include more attributes other than symbol.
	//Will need to implement a comparator in the exchange if so, as there we need
	//to compare on symbol only.
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rate other = (Rate) obj;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}
	
	
	
}
