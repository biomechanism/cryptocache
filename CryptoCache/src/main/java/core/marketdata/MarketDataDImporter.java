/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.marketdata;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.binance.api.client.domain.market.Candlestick;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

public class MarketDataDImporter {

	public IMarketData importData(String filename) {

		try (FileReader filereader = new FileReader(filename);
				CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build()) {
			List<String[]> allData = csvReader.readAll();

			return process(allData);

		} catch (IOException | CsvException e) {
			throw new RuntimeException(e);
		}
	}

	private IMarketData process(List<String[]> data) {
		List<Rate> rates = data.stream().map(e -> new Rate(e[1].trim())).collect(Collectors.toList());
		return new MarketData(rates);
	}
	
	
	public void exportClosingPrices(List<Candlestick> candlesticks, String filename) {
		try (FileWriter writer = new FileWriter(filename);) {
			
			for (Candlestick candle : candlesticks) {
				String output = outputFormat(candle);
				writer.write(output);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	
	private String outputFormat(Candlestick candlestick) {
		
		Long closeTime = candlestick.getCloseTime();
		String closePrice = candlestick.getClose();
		
		String formatted = String.format("%d, %s%n", closeTime, closePrice);

		return formatted;
		
	}

	public IMarketData importCandleData(String candleMdFile) {

		IMarketData marketData = new MarketData();
		List<Candlestick> candles = new ArrayList<>();
		
		try (FileReader filereader = new FileReader(candleMdFile);
				CSVReader csvReader = new CSVReaderBuilder(filereader).withSkipLines(1).build()) {
			List<String[]> allData = csvReader.readAll();

			
			for (String[] e : allData) {
				Candlestick candle = createCandlestick(e[0], e[1]);
				candles.add(candle);
			}
			
			marketData.setCandles(candles);
			
			List<Rate> rates = candles.stream().map(e -> new Rate(e.getClose().trim())).collect(Collectors.toList());
			marketData.setRates(rates);
			
			
			return marketData;

		} catch (IOException | CsvException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private Candlestick createCandlestick(String closeTime, String closePrice) {
		Candlestick candle = new Candlestick();
		candle.setCloseTime(Long.valueOf(closeTime));
		candle.setClose(closePrice);
		return candle;
		
	}

}
