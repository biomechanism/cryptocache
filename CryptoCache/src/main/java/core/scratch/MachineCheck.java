/*******************************************************************************
 * Copyright (C) 2021 Shane O'Neill
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package core.scratch;

import core.autotrader.IMachineState;

public class MachineCheck {

	public static void main(String[] args) {
		
		new MachineCheck().init();


	}
	
	private void init() {
		
		Machine1 m1 = new Machine1();
		Machine2 m2 = new Machine2();
		
		System.out.println("M1 is State M1.BUY = " + m1.getState().isState(Machine1State.BUY));
		System.out.println("M1 is State M2.BUY = " + m1.getState().isState(Machine2State.BUY));
		
	}
	
	class Machine1 {
		IMState state = Machine1State.BUY;
		
		public IMState getState() {
			return state;
		}
		
	}
	
	
	class Machine2 {
		IMState state = Machine2State.BUY;
		
		public IMState getState() {
			return state;
		}
		
	}
	
	interface IMState {
		boolean isState(IMState state);
	}
	
	enum Machine1State implements IMState {
		BUY, SELL;

		@Override
		public boolean isState(IMState state) {
			return this.toString().equals(state.toString());
		}
		
	}
	
	enum Machine2State implements IMState {
		BUY, SELL, TERMINATE;

		@Override
		public boolean isState(IMState state) {
			return this.toString().equals(state.toString());
		}
		
	}

}
